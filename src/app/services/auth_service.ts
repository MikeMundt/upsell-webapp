import {Injectable, NgZone} from '@angular/core';
import {Router} from '@angular/router';
import { Observable }     from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { config } from '../config';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { JwtHelper } from 'angular2-jwt';

@Injectable()
export class AuthService {

  private _loginUrl = config.getEnvironmentVariable('endPoint')+'auth/token';
  private _jwtHelper: JwtHelper = new JwtHelper();
  constructor(private http: Http){
    this.http = http;

  }

  public login(username, password) {
    //if (this.isLoggedIn()) return;

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });

    return this.http
      .post(
        this._loginUrl,
        JSON.stringify({'userName': username, 'password': password}),
      options
      )
      .map(
        response => {
          this.storeToken(response.json().jwt);
      })
      .catch(this.handleError);
  }

  public isLoggedIn() {
    return (localStorage.getItem('id_token'));
  }
  private storeToken(token){
    localStorage.setItem('id_token', token);
    localStorage.setItem('user', JSON.stringify(this._jwtHelper.decodeToken(token)));

  }

  public getTokenInfo() {
    return JSON.parse(localStorage.getItem('user'));
  }

  public getPerms(){
    return this.getTokenInfo() ? this.getTokenInfo()['perm'] : [];
  }

  public isAdmin(){
    let admin = false
    this.getPerms().forEach((e) => {
      if (e === 'TENANT_ADMIN' || e === 'SYS_ADMIN')
      {
        admin = true
      }
    })
    return admin
  }

  private handleError (error: any) {
    // In a real world app, we might use a remote logging infrastructure
    // We'd also dig deeper into the error to get a better message
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg); // log to console instead
    return Observable.throw(errMsg);
  }
}
