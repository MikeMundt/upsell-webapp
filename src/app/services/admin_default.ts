import {Injectable, NgZone} from '@angular/core';
import {Http, Headers, Response, RequestOptions} from '@angular/http';
import {config} from '../config';

import {Observable} from 'rxjs/Rx';


@Injectable()
export class AdminDefault {

    private _url = config.getEnvironmentVariable('endPoint') + 'admin/';

    constructor(public http: Http) {
    }

    getDOW(hotel: any) {

        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('id_token'));
        let options = new RequestOptions({headers: headers});
        let url = this._url + hotel + '/default/dow';

        return this.http.get(url, options).map(r => r.json()).catch(this.handleError)

    }

    updateInventory(hotel: any, room_category_id: any, dt_stay: any, avail_cd: any) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('id_token'));
        let options = new RequestOptions({headers: headers});
        let parameters = {room_category_id: room_category_id, dt_stay: dt_stay, avail_cd: avail_cd};
        let url = this._url + hotel + '/availability';

        //this._url = this._url+ hotel + '/availability'

        let body = JSON.stringify(parameters);

        return this.http.post(url, body,
            options
        )
            .map(r => r.json())
            .catch(this.handleError);
    }


    getGeneric(hotel: any, data: string) {

        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('id_token'));
        let options = new RequestOptions({headers: headers});
        let url = this._url + hotel + '/default/' + data;

        return this.http.get(url, options).map(r => r.json()).catch(this.handleError)

    }

    putGeneric(hotel: any, data: string, item_id:string, postObj: any) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('id_token'));
        let options = new RequestOptions({headers: headers});
        //let parameters = {room_category_id: room_category_id, dt_stay: dt_stay, avail_cd: avail_cd};
        let url = this._url + hotel + '/default/' + data + '/' + item_id;

        let body = JSON.stringify(postObj);

        return this.http.put(url, body,
            options
        )
            .map(r => r.json())
            .catch(this.handleError);
    }



    private handleError(error: any) {
        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg); // log to console instead
        return Observable.throw(errMsg);
    }

}

