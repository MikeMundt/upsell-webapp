import {Component, OnInit, OnChanges} from '@angular/core';

import {Shared} from '../../services/shared'
import {AdminDefault} from '../../services/admin_default'


declare var module: any; //needed for moduleId
declare var $: any;

@Component({
    moduleId: module.id,
    selector: 'luxe-admin-default-adu',
    templateUrl: "../../templates/admin/admin_default_adu.template.html",
    providers: [AdminDefault]
})
export class AdminDefaultAduComponent implements OnInit {
    data: any;
    errorMessage: any;

    constructor(private shared: Shared, private adminDefault: AdminDefault) {
        this.shared = shared;
        this.adminDefault = adminDefault;
    }

    ngOnInit () {
        this.adminDefault.getGeneric(this.shared.selectedHotel, 'adu').subscribe(
            d => {
                this.data = d;
                this.data.forEach(function(x) {
                    x.adu_desc = '$' + x.lower_range + ' - $' + x.upper_range;
                });
            },
            error => this.errorMessage = <any>error);


    }

    clicked(event, val: any) {
        if (!event) event = window.event;
        let tar = event.target || event.srcElement;
        let new_value = tar.innerText;

        if (isNaN(new_value) || Number(new_value) < 0 || Number(new_value) > 2 || Number(new_value) == Number(val.factor_man)) {
            tar.innerText = parseFloat(val.factor_man).toFixed(2);
            return false;
        }

        let putObject = { factor_val: 0 };
        if (!isNaN(parseFloat(new_value)) && isFinite(new_value)) {
            putObject.factor_val = new_value;
            this.adminDefault.putGeneric(this.shared.selectedHotel, 'adu', val.id, putObject).subscribe(
                d => {
                    val.factor_man = new_value;
                    tar.innerText = parseFloat(val.factor_man).toFixed(2);
                },
                error => this.errorMessage = <any>error);

        }
        else {
            putObject.factor_val = 0;
            this.adminDefault.putGeneric(this.shared.selectedHotel, 'adu', val.id, putObject).subscribe(
                d => {
                    val.factor_man = 0;
                    tar.innerText = parseFloat(val.factor_man).toFixed(2);
                },
                error => this.errorMessage = <any>error);

            putObject.factor_val = 0;

        }

    }

    highlight(event:any) {
        event.preventDefault();
        var range = document.createRange();
        range.selectNodeContents(event.currentTarget);
        var sel = window.getSelection();
        sel.removeAllRanges();
        sel.addRange(range);
        event.currentTarget.focus();
    }

}
