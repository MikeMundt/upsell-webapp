import {Component, OnInit, OnChanges} from '@angular/core';

import {Shared} from '../../services/shared'
import {Allocation} from '../../services/allocation'


declare var module: any; //needed for moduleId
declare var $: any;

@Component({
    moduleId: module.id,
    selector: 'luxe-admin-allocation-default',
    templateUrl: "../../templates/admin/admin_allocation_default.template.html",
    providers: [Allocation]
})
export class AdminAllocationDefaultComponent implements OnInit {
    data: any;
    errorMessage: any;

    constructor(private shared: Shared, private allocation: Allocation) {
        this.shared = shared;
        this.allocation = allocation;
    }

    ngOnInit () {
        this.allocation.getAllocationDefault(this.shared.selectedHotel).subscribe(
            d => {
                this.data = d;
                this.data.sort( function(d1, d2) {
                    if ( Number(d1.display_order) < Number(d2.display_order) ){
                        return -1;
                    }else if( Number(d1.display_order) > Number(d2.display_order) ){
                        return 1;
                    }else{
                        return 0;
                    }
                });
            },
            error => this.errorMessage = <any>error);

    }

    checkEnter(event) {
        if (event.keyCode === 13) {
            event.target.blur();
            event.preventDefault();
        }
    }

    clicked(event, roomCategory: any, defaultAllocation: any) {
        if (!event) event = window.event;
        let tar = event.target || event.srcElement;
        let new_value = tar.innerText.replace('$', "");

        if (new_value == '' || isNaN(new_value) || Number(new_value) == Number(defaultAllocation.allocation_default)) {
            tar.innerText = parseFloat(defaultAllocation.allocation_default).toFixed(0);
            return false;
        }


        if (!isNaN(parseFloat(new_value)) && isFinite(new_value)) {
            this.allocation.updateAllocationDefault(this.shared.selectedHotel, roomCategory.room_category_id, defaultAllocation.dow, new_value).subscribe(
                d => {
                    defaultAllocation.allocation_default = new_value;
                    tar.innerText = parseFloat(defaultAllocation.allocation_default).toFixed(0);
                },
                error => this.errorMessage = <any>error);


        }
       /* else {
            putObject.price_bar = 0;
            this.adminDefault.putGeneric(this.shared.selectedHotel, 'bar', val.id, putObject).subscribe(
                d => {
                    val.price_bar = 0;
                    tar.innerText = '$' + parseFloat(val.price_bar).toFixed(0);
                },
                error => this.errorMessage = <any>error);

        }*/

    }

    highlight(event:any) {
        event.preventDefault();
        var range = document.createRange();
        range.selectNodeContents(event.currentTarget);
        var sel = window.getSelection();
        sel.removeAllRanges();
        sel.addRange(range);
        event.currentTarget.focus();
    }

}
