import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule, CanActivate } from '@angular/router';
import {HotelComponent} from './components/hotels';
import { LoginComponent }   from './components/login';
import { ConfirmationComponent }   from './components/confirmation';
import { AdminInventoryComponent }   from './components/admin/admin_inventory';
import { AdminAllocationComponent }   from './components/admin/admin_allocation';
import { AdminAllocationDefaultComponent }   from './components/admin/admin_allocation_default';
import { AdminInventoryAlertsComponent }   from './components/admin/admin_inventory_alerts';
import { AdminComponent }   from './components/admin/admin';
import { AdminGeneralComponent }   from './components/admin/admin_general';
import { AdminPricingDefaultComponent }   from './components/admin/admin_pricing_default';
import { AdminPricingBarComponent }   from './components/admin/admin_pricing_bar';
import { AdminPricingDiscComponent }   from './components/admin/admin_pricing_disc';
import { AdminReportingComponent } from './components/admin/admin_reporting';
import { AdminDefaultDowComponent } from './components/admin/admin_default_dow';
import { AdminDefaultLosComponent } from './components/admin/admin_default_los';
import { AdminDefaultToaComponent } from './components/admin/admin_default_toa';
import { AdminDefaultAduComponent } from './components/admin/admin_default_adu';
import { AdminDefaultLimitsComponent } from './components/admin/admin_default_limits';
import { AdminDefaultBarComponent } from './components/admin/admin_default_bar';
import { GenericComponent }   from './components/generic';
import { OfferGenericComponent }   from './components/offer_generic';
import { ConfirmationVerifyComponent }   from './components/confirmation_verify';
import { AuthGuard } from './services/auth_guard_service';
import { AdminPromoComponent } from './components/admin/admin_promo';

import { ProfileComponent }   from './components/profile';
import { RecoveryComponent }   from './components/recovery';

const appRoutes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'admin', component: AdminComponent, canActivate: [AuthGuard],
    children: [
        { path: '', redirectTo: 'inventory', pathMatch: 'full' },
        { path: 'general', component: AdminGeneralComponent },
        { path: 'inventory', component: AdminInventoryComponent, canActivate: [AuthGuard] },
        { path: 'allocation', component: AdminAllocationComponent, canActivate: [AuthGuard] },
        { path: 'allocation_default', component: AdminAllocationDefaultComponent, canActivate: [AuthGuard] },
        { path: 'alerts', component: AdminInventoryAlertsComponent, canActivate: [AuthGuard] },
        { path: 'pricing', component: AdminPricingDefaultComponent, canActivate: [AuthGuard] },
        { path: 'bar', component: AdminPricingBarComponent, canActivate: [AuthGuard] },
        { path: 'disc', component: AdminPricingDiscComponent, canActivate: [AuthGuard] },
        { path: 'reporting', component: AdminReportingComponent, canActivate: [AuthGuard]},
        { path: 'default_ofr', component: AdminDefaultDowComponent, canActivate: [AuthGuard]},
        { path: 'default_los', component: AdminDefaultLosComponent, canActivate: [AuthGuard]},
        { path: 'default_toa', component: AdminDefaultToaComponent, canActivate: [AuthGuard]},
        { path: 'default_adu', component: AdminDefaultAduComponent, canActivate: [AuthGuard]},
        { path: 'default_limits', component: AdminDefaultLimitsComponent, canActivate: [AuthGuard]},
        { path: 'default_bar', component: AdminDefaultBarComponent, canActivate: [AuthGuard]},
        { path: 'promo', component: AdminPromoComponent, canActivate: [AuthGuard]}
    ]
  },
  { path: 'confirm', component: ConfirmationComponent },
  { path: 'hotels', component: HotelComponent },
//  { path: 'hotel/:id', component: RoomClassComponent },
//  { path: 'hotel/:id/:class', component: RoomTypeComponent },
//  { path: 'hotel/:id/:class/:type', component: LengthOfStayComponent },
//  { path: 'hotel/:id/:class/:type/:lengthofstay', component: SourceCodeComponent },
  { path: 'generic/:hotel_id', component: GenericComponent },
//  { path: 'generic/:hotel_id/**', component: GenericComponent },
  { path: 'offer_generic/:id', component: OfferGenericComponent },
  { path: 'offer_generic', component: OfferGenericComponent },
//  { path: 'hotel/:id/:class/:type/:lengthofstay/:first_letter', component: SourceCodeComponent },
//  { path: 'offer/:id/:class/:type/:lengthofstay/:source_id', component: OfferComponent },
//  { path: 'offer/:id/:confirmation', component: OfferComponent },
  { path: 'confirmation/:id/:confirmation', component: ConfirmationVerifyComponent },
    { path: 'profile', component: ProfileComponent },
    { path: 'recovery/:token_id', component: RecoveryComponent }
];

export const appRoutingProviders: any[] = [

];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);

