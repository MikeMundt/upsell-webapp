import {Component, OnInit} from '@angular/core';

import {Shared} from '../../services/shared'
import {Reporting} from '../../services/reporting'

declare var module: any; //needed for moduleId
declare var $: any;

@Component({
    moduleId: module.id,
    selector: 'luxe-admin-reporting',
    templateUrl: "../../templates/admin/admin_reporting.template.html",
    providers: [Reporting]
})
export class AdminReportingComponent implements OnInit {
    data: any[];
    total: {};
    errorMessage: string;
    useAgent: boolean;
    currentSpan: string;

    constructor(private shared: Shared,private reporting: Reporting) {
        this.shared = shared;
        this.useAgent = true;
        this.currentSpan = 'TDY';
        this.reporting = reporting;

    }

    ngOnInit () {
        this.updateData();
    }

    updateData() {
        this.data = [];
        this.total = { num_checkins: 0, num_upsells: 0, upsell_room_nights: 0, upsell_revenue: 0, upsell_bar_revenue: 0,  };

        if (this.useAgent) {
            this.reporting.getAgent(this.shared.selectedHotel, this.currentSpan).subscribe(
                data => {

                    data.data.map(function (rec) {
                        rec.avg_los = rec.num_upsells > 0 ? rec.upsell_room_nights / rec.num_upsells : 0;
                        rec.rev_per_upsell = rec.num_upsells > 0 ? Number(rec.upsell_revenue) / rec.num_upsells : 0;
                        rec.conversion = rec.num_checkins > 0 ? rec.num_upsells / rec.num_checkins : 0;
                        rec.conversion = Number(rec.conversion).toFixed(3);
                        rec.rev_per_bar = rec.num_checkins > 0 ? Number(rec.upsell_bar_revenue) / rec.num_checkins : 0;
                        rec.total_rev = Number(rec.upsell_revenue) + Number(rec.upsell_bar_revenue);
                    });

                    for (let rec of data.data) {
                        this.total['num_checkins'] += Number(rec['num_checkins']);
                        this.total['num_upsells'] += Number(rec['num_upsells']);
                        this.total['upsell_room_nights'] += Number(rec['upsell_room_nights']);
                        this.total['upsell_revenue'] += Number(rec['upsell_revenue']);
                        this.total['upsell_bar_revenue'] += Number(rec['upsell_bar_revenue']);
                    }
                    this.total['avg_los'] = this.total['num_upsells'] > 0 ? this.total['upsell_room_nights'] / this.total['num_upsells'] : 0;
                    this.total['rev_per_upsell'] = this.total['num_upsells'] > 0 ? this.total['upsell_revenue'] / this.total['num_upsells'] : 0;
                    this.total['conversion'] = this.total['num_checkins'] > 0 ? this.total['num_upsells'] / this.total['num_checkins'] : 0;
                    this.total['conversion'] = Number(this.total['conversion']).toFixed(3);
                    this.total['rev_per_bar'] = this.total['num_checkins'] > 0 ? this.total['upsell_bar_revenue'] / this.total['num_checkins'] : 0;
                    this.total['total_rev'] = this.total['upsell_revenue'] + this.total['upsell_bar_revenue'];

                    this.data = data.data;
                },
                error => this.errorMessage = <any>error);
        } else {
            this.reporting.getRoom(this.shared.selectedHotel, this.currentSpan).subscribe(
                data => {

                    data.data.map(function (rec) {
                        rec.avg_los = rec.num_upsells > 0 ? rec.upsell_room_nights / rec.num_upsells : 0;
                        rec.rev_per_upsell = rec.num_upsells > 0 ? Number(rec.upsell_revenue) / rec.num_upsells : 0;
                        rec.total_rev = Number(rec.upsell_revenue) + Number(rec.upsell_bar_revenue);
                    });

                    for (let rec of data.data) {
                        this.total['num_upsells'] += Number(rec['num_upsells']);
                        this.total['upsell_room_nights'] += Number(rec['upsell_room_nights']);
                        this.total['upsell_revenue'] += Number(rec['upsell_revenue']);
                        this.total['upsell_bar_revenue'] += Number(rec['upsell_bar_revenue']);
                    }
                    this.total['avg_los'] = this.total['num_upsells'] > 0 ? this.total['upsell_room_nights'] / this.total['num_upsells'] : 0;
                    this.total['rev_per_upsell'] = this.total['num_upsells'] > 0 ? this.total['upsell_revenue'] / this.total['num_upsells'] : 0;
                    this.total['total_rev'] = this.total['upsell_revenue'] + this.total['upsell_bar_revenue'];

                    this.data = data.data.sort((n1, n2) => n1.display_order - n2.display_order);;
                },
                error => this.errorMessage = <any>error);
        }

    }

    changeView() {
        this.useAgent = !this.useAgent;
        this.updateData();
    }

    changeSpan(span: string) {
        this.currentSpan = span;
        this.updateData();
    }
}