import { Serializable } from './serializable';

export class RoomType extends Serializable {
    type: string;
    description: string;
    name: string;
}
