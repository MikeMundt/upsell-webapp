import { Component, OnInit, Injectable } from '@angular/core';
import { Hotels } from '../services/hotels'
import { TreePath } from '../services/tree_path'
import { Hotel } from '../interfaces/hotel'
import { config } from '../config'
import {
  CanActivate,
  Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  ActivatedRoute,
  NavigationEnd,

} from '@angular/router';
import { Location } from '@angular/common';
import { Shared }   from '../services/shared';

declare var module: any;
@Component({
    moduleId: module.id,
    selector: 'luxe-radial-upsell',
    templateUrl: '../templates/generic.template.html',
    providers: [Hotels, TreePath]
})
export class GenericComponent implements OnInit {
    errorMessage: string;
    hotels: Hotel[];
    values: Array<any>;
    variable_name: string;
    tree: any;
    bread: Array<any>;
    showPromo: boolean;

    constructor(private hotelService : Hotels,  private router: Router, private shared: Shared, private treePath: TreePath, private activatedRoute: ActivatedRoute, private location: Location) {
      this.hotelService = hotelService;
      this.router = router;
      this.shared = shared;
      this.shared.show_res_conf = false;
      this.showPromo = false;
    }

    ngOnInit() {
      this.init();

    }

    init() {
      this.treePath.getTreePath(this.shared.selectedHotel).subscribe(
        tree => {

          if (tree.tree_path.length) {
            this.values = tree.tree_path[0].values;
            this.variable_name = tree.tree_path[0].variable_name;
            this.tree = tree;
            this.bread = [];
            this.shared.params = {};
            this.bread.push({display: tree.tree_path[0].variable_display_name, selection: null, position: 0, listObject: 0, values: tree.tree_path[0].values });
          }
        }
      );

      this.treePath.getQuickClick(this.shared.selectedHotel).subscribe(
        quick => {
          this.shared.quickClickData = quick;
        }
      );
    }

    advance(value: any) {
      this.shared.params[this.variable_name] = value.value_id;
      var cur = this.bread[this.bread.length - 1];
      cur.selection = value.value_display_name;

      if (value && value.values) {
        this.bread.push({display: value.variable_display_name, selection: null, position: cur.position + 1, listObject: cur.listObject, values: value.values });
        this.variable_name = value.variable_name;
        this.values = value.values;
      } else if (this.tree.tree_path.length - 1 > cur.listObject) {

        let newLevel = this.tree.tree_path[cur.listObject + 1];
        this.variable_name = newLevel.variable_name;
        this.bread.push({display: newLevel.variable_display_name, selection: null, position: cur.position + 1, listObject: cur.listObject + 1, values: newLevel.values });


        this.values = newLevel.values ? newLevel.values : null;

      } else {
        this.navigate();
      }
    }


    quickClick(roomId: string, value: any) {
        this.shared.params['base_room_category_id'] = roomId;
        this.shared.params['los'] = value.value_display_name;
        this.navigate();
    }

    clickBread(event: any, item: any) {
      event.preventDefault();

      this.bread.splice(item.position + 1, this.bread.length - item.position);

      let cur = this.bread[this.bread.length - 1];
      cur.selection = null;
      this.values = cur.values;
    }

    navigate() {
        if (this.shared.usePromo) {
            this.showPromo = true;
            setTimeout(function(){
                document.getElementById("txtPromoCode").focus();
            }, 200);

        } else {
            this.getOffer();
        }

    }

    getOffer() {
        if (this.shared.usePromo) {
            this.shared.params['promo_cd'] = this.shared.promoCode.toUpperCase();
        }

        this.shared.last_url = this.router.url;
        this.router.navigate(['offer_generic', this.shared.selectedHotel]);
    }

}
