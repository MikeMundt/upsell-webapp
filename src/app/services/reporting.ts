import {Injectable, NgZone} from '@angular/core';
import {Http, Headers, Response, RequestOptions} from '@angular/http';
import {config} from '../config';

import {Observable} from 'rxjs/Rx';


@Injectable()
export class Reporting {

    private _url = config.getEnvironmentVariable('endPoint') + 'admin/';

    constructor(public http: Http) {
    }

    getAgent(hotel: any, dateRange: string) {

        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('id_token'));
        let options = new RequestOptions({headers: headers});
        let url = this._url + hotel + '/reporting/agent_performance?date_range=' + dateRange;

        return this.http.get(url, options).map(this.setData).catch(this.handleError)

    }

    getRoom(hotel: any, dateRange: string) {

        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('id_token'));
        let options = new RequestOptions({headers: headers});
        let url = this._url + hotel + '/reporting/room_category_performance?date_range=' + dateRange;

        return this.http.get(url, options).map(this.setData).catch(this.handleError)

    }

    private setData(data) {
        //console.log(data.json().sort((n1, n2) => n1.display_order - n2.display_order));
        return data.json();
    }

    private setDataSort(data) {
        //console.log(data.json().sort((n1, n2) => n1.display_order - n2.display_order));
        return data.json().data.sort((n1, n2) => n1.display_order - n2.display_order);
    }


    private handleError(error: any) {
        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg); // log to console instead
        return Observable.throw(errMsg);
    }

}

