import {Injectable, NgZone} from '@angular/core';
import {Http, Headers, Response, RequestOptions} from '@angular/http';
import {config} from '../config';

import {Observable} from 'rxjs/Rx';


@Injectable()
export class Account {

    private _url = config.getEnvironmentVariable('endPoint') + 'account/';

    constructor(public http: Http) {
    }

    updatePassword(oldPassword: string, newPassword: string) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('id_token'));
        let options = new RequestOptions({headers: headers});
        let parameters = {oldPassword: oldPassword, newPassword: newPassword};
        let url = this._url + 'passwordreset';

        let body = JSON.stringify(parameters);

        return this.http.put(url, body,
            options
        )
            .map(r => r.json())
            .catch(this.handleError);

    }

    userRecovery(emailAddress: string) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('id_token'));
        let options = new RequestOptions({headers: headers});
        let parameters = {emailAddress: emailAddress};
        let url = this._url + 'recovery';

        //this._url = this._url+ hotel + '/availability'

        let body = JSON.stringify(parameters);

        return this.http.post(url, body,
            options
        )
            .map(r => r.json())
            .catch(this.handleError);
    }

    tokenValidation(token: string) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('id_token'));
        let options = new RequestOptions({headers: headers});
        let parameters = {token: token};
        let url = this._url + 'token_validation';

        //this._url = this._url+ hotel + '/availability'

        let body = JSON.stringify(parameters);

        return this.http.put(url, body,
            options
        )
            .map(r => r.json())
            .catch(this.handleError);
    }

    tokenSubmit(token: string, password: string) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('id_token'));
        let options = new RequestOptions({headers: headers});
        let parameters = {token: token, password: password};
        let url = this._url + 'token_submit';

        //this._url = this._url+ hotel + '/availability'

        let body = JSON.stringify(parameters);

        return this.http.post(url, body,
            options
        )
            .map(r => r.json())
            .catch(this.handleError);
    }

    private handleError(error: any) {
        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg); // log to console instead
        return Observable.throw(errMsg);
    }

}

