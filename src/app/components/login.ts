import { Component, OnInit, Injectable, ViewChild, NgZone } from '@angular/core';
import { AuthService } from '../services/auth_service'
import { Account } from '../services/account'
import { config } from '../config'
import {
  CanActivate,
  Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  ActivatedRoute
} from '@angular/router';

import { Shared }   from '../services/shared';

declare var module: any; //needed for moduleId
@Component({
    moduleId: module.id,
    selector: 'login',
    templateUrl: '../templates/login.template.html',
    providers: [AuthService, Account]
})
export class LoginComponent implements OnInit {
    errorMessage: string;
    username: string;
    password: string;
    showRecovery: boolean = false;
    userEmail: string;
    recoverySuccessful: boolean = false;
    @ViewChild('password') passwordInput;
    @ViewChild('username') usernameInput;
    constructor(private loginService : AuthService, private accountService : Account, private router: Router, private shared: Shared, private _ngZone: NgZone) {
        this.accountService = accountService;
      this.loginService = loginService;
      this.router = router;
      this.shared = shared;

      localStorage.clear();
    }

    onUKey(username: string) {
      this.username = username
    }

    onPKey(password: string, event: any) {
      this.password = password
      if(event && event.keyCode && event.keyCode == 13)
        this.login()
    }

    ngOnInit() {

    }

    login() {
      this.loginService.login(this.usernameInput.nativeElement.value, this.passwordInput.nativeElement.value).subscribe( () => {
          this.router.navigate(["/hotels"])
          this.shared.username = this.usernameInput.nativeElement.value;
      },
      error =>  {
        this.errorMessage = "Unable to login, please check username and password"
      })
    }

    toggleRecovery() {
        this.showRecovery = !this.showRecovery;
        this.recoverySuccessful = false;
        this.userEmail = null;
    }

    recoverPassword() {
        this.accountService.userRecovery(this.userEmail).subscribe( () => {
                this.recoverySuccessful = true;
            },
            error =>  {
                this.errorMessage = ""
            })
    }

}
