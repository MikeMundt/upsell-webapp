import {Injectable, NgZone} from '@angular/core';
import {Http, Headers, Response, RequestOptions} from '@angular/http';
import {config} from '../config';

import {Hotel} from '../interfaces/hotel';
import {Serializable} from '../interfaces/serializable';

import {Observable} from 'rxjs/Rx';


@Injectable()
export class Inventory {

    private _url = config.getEnvironmentVariable('endPoint') + 'hotel/';
    private _availableHotels: Array<Hotel> = [];

    constructor(public http: Http) {
    }

    getInventory(hotel: any, page: number) {

        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('id_token'));
        let options = new RequestOptions({headers: headers});
        let url = this._url + hotel + '/availability?page=' + page;

        return this.http.get(url, options).map(this.setInventory).catch(this.handleError)

    }

    updateInventory(hotel: any, room_category_id: any, dt_stay: any, avail_cd: any) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('id_token'));
        let options = new RequestOptions({headers: headers});
        let parameters = {room_category_id: room_category_id, dt_stay: dt_stay, avail_cd: avail_cd};
        let url = this._url + hotel + '/availability';

        //this._url = this._url+ hotel + '/availability'

        let body = JSON.stringify(parameters);

        return this.http.post(url, body,
            options
        )
            .map(r => r.json())
            .catch(this.handleError);
    }


    private setInventory(data) {
        //console.log(data.json().sort((n1, n2) => n1.display_order - n2.display_order));
        return data.json().sort((n1, n2) => n1.display_order - n2.display_order);
    }


    private handleError(error: any) {
        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg); // log to console instead
        return Observable.throw(errMsg);
    }

}

