export class Serializable {
    fillFromJSON(json: string) {
        var jsonObj = JSON.parse(json);
        for (var propName in jsonObj) {
            this[propName] = jsonObj[propName];
        }
    }

    fillFromObject(inObject) {
        for (var propName in inObject) {
            this[propName] = inObject[propName];
        }
    }
}
