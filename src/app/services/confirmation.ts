import {Injectable, NgZone} from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { config } from '../config';


import { Observable } from 'rxjs/Rx';


@Injectable()
export class ConfirmationService {

  private _url = config.getEnvironmentVariable('endPoint')+'';

  constructor(public http: Http) {
  }

   getConfirmation(confirmation_id, hotel_id) {

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', 'Bearer ' + localStorage.getItem('id_token'))
    let options = new RequestOptions({ headers: headers });


    return this.http.get(this._url+'hotel/' + hotel_id + '/reservation/' + confirmation_id,
      options
      )
      .map(this.setConfirmation)
      .catch(this.handleError)

  }
  
  private setConfirmation(data)
  {
    return data.json()
  }

  private handleError (error: any) {
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg); // log to console instead
    return Observable.throw(errMsg);
  }

}
