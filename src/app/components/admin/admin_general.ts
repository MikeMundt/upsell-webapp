import {Component, OnInit, OnChanges} from '@angular/core';

import {Shared} from '../../services/shared'
import {Inventory} from '../../services/inventory'


declare var module: any; //needed for moduleId
declare var $: any;

@Component({
    moduleId: module.id,
    selector: 'luxe-admin-general',
    templateUrl: "../../templates/admin/admin_general.template.html",
    providers: [Inventory]
})
export class AdminGeneralComponent implements OnInit {
    columnData: any;
    rowData: any;
    errorMessage: any;
    cells: any;

    constructor(private shared: Shared) {
         this.shared = shared;
    }

    ngOnInit () {

    }
    onChange(event:any){
      console.log('changed');
    }

}
