import {Component, OnInit, OnChanges} from '@angular/core';

import {Shared} from '../../services/shared'
import {Inventory} from '../../services/inventory'

import {AdminRoomInventory} from '../../interfaces/adminRoomInventory';

import {inventoryLevel} from '../../helpers';

declare var module: any; //needed for moduleId
declare var $: any;

@Component({
    moduleId: module.id,
    selector: 'luxe-admin-inventory-alerts',
    templateUrl: "../../templates/admin/admin_inventory_alerts.template.html",
    providers: [Inventory]
})
export class AdminInventoryAlertsComponent implements OnInit {
    alerts: any[];

    constructor(private shared: Shared) {
        this.shared = shared;
        this.alerts = [];
    }

    ngOnInit() {
        let that = this;

    }

    create(type: string) {
        let idx = this.alerts.length + 1;
        let newAlert = { id: idx, contactInfo: '', type: type, alertRunsOut: true, alertReached: false, isEdit: true, active: true, placeHolder: 'Enter Email' };
        if (type == "TEXT") newAlert.placeHolder = "(xxx)xxx-xxxx";
        this.alerts.push(newAlert);
    }

    edit(alert: any) {
        alert.isEdit = true;
    }

    save(alert: any) {
        alert.isEdit = false;
    }

    delete(alert: any) {
        let index = this.alerts.findIndex(d => d.id === alert.id); //find index in your array
        this.alerts.splice(index, 1);//remove element from array
    }

    test(alert: any) {

    }


}
