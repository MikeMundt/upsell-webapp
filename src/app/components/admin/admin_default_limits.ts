import {Component, OnInit, OnChanges} from '@angular/core';

import {Shared} from '../../services/shared'
import {AdminDefault} from '../../services/admin_default'


declare var module: any; //needed for moduleId
declare var $: any;

@Component({
    moduleId: module.id,
    selector: 'luxe-admin-default-limits',
    templateUrl: "../../templates/admin/admin_default_limits.template.html",
    providers: [AdminDefault]
})
export class AdminDefaultLimitsComponent implements OnInit {
    data: any;
    errorMessage: any;

    constructor(private shared: Shared, private adminDefault: AdminDefault) {
        this.shared = shared;
        this.adminDefault = adminDefault;
    }

    ngOnInit () {
        this.adminDefault.getGeneric(this.shared.selectedHotel, 'limits').subscribe(
            d => {
                this.data = d;
                this.data.sort( function(d1, d2) {
                    if ( d1.display_order < d2.display_order ){
                        return -1;
                    }else if( d1.display_order > d2.display_order ){
                        return 1;
                    }else{
                        return 0;
                    }
                });
            },
            error => this.errorMessage = <any>error);
    }

    clicked(event, val: any, field: string) {
        if (!event) event = window.event;
        let tar = event.target || event.srcElement;
        let new_value = tar.innerText;
        let curField = field == 'min' ? val.min_factor : val.max_factor;

        if (isNaN(new_value) || Number(new_value) < 0 || Number(new_value) > 2 || Number(new_value) == Number(curField)) {
            tar.innerText = parseFloat(curField).toFixed(2);
            return false;
        }

        let putObject = { factor_min: val.min_factor, factor_max: val.max_factor };
        //let xfield = field == 'min' ? putObject.factor_min : putObject.factor_max;
        if (!isNaN(parseFloat(new_value)) && isFinite(new_value)) {
            if (field == 'min') {
                putObject.factor_min = new_value;
            } else {
                putObject.factor_max = new_value;
            }
            this.adminDefault.putGeneric(this.shared.selectedHotel, 'limits', val.room_category_id, putObject).subscribe(
                d => {
                    curField = new_value;
                    tar.innerText = parseFloat(curField).toFixed(2);
                },
                error => this.errorMessage = <any>error);


        }
        else {
            if (field == 'min') {
                putObject.factor_min = 0;
            } else {
                putObject.factor_max = 0;
            }
            this.adminDefault.putGeneric(this.shared.selectedHotel, 'limits', val.room_category_id, putObject).subscribe(
                d => {
                    curField = 0;
                    tar.innerText = parseFloat(curField).toFixed(2);
                },
                error => this.errorMessage = <any>error);

        }

    }

    highlight(event:any) {
        event.preventDefault();
        var range = document.createRange();
        range.selectNodeContents(event.currentTarget);
        var sel = window.getSelection();
        sel.removeAllRanges();
        sel.addRange(range);
        event.currentTarget.focus();
    }

}
