import {Injectable, NgZone} from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { config } from '../config';

import { Hotel } from '../interfaces/hotel';

import { Observable } from 'rxjs/Rx';


@Injectable()
export class OfferService {

  private offer;
  private errorMessage;
  private _url = config.getEnvironmentVariable('endPoint')+'upsell/';
  constructor(public http: Http) {

    this.http = http;

  }

  getOfferGeneric(hotel_id: string, parameters: any) {


    parameters.hotel_id = hotel_id
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', 'Bearer ' + localStorage.getItem('id_token'));
    let options = new RequestOptions({ headers: headers });

    let body = JSON.stringify(parameters);
    return this.http.post(this._url,body,
      options
      )
      .map(this.setOffer)
      .catch(this.handleError)

  }

  getOffer(id: number, hotel_id: number) {

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', 'Bearer ' + localStorage.getItem('id_token'));
    let options = new RequestOptions({ headers: headers });

    let body = JSON.stringify({"conf_num": id, "hotel_id": hotel_id });
    return this.http.post(this._url,body,
      options
      )
      .map(this.setOffer)
      .catch(this.handleError)

  }

  getOfferByTree(hotel_id: number, class_id: string, room_type: string, length_of_stay: number, source_id: string ) {

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', 'Bearer ' + localStorage.getItem('id_token'));
    let options = new RequestOptions({ headers: headers });

    let body = JSON.stringify({"hotel_id": hotel_id, "base_room_category_id":  room_type, "los": length_of_stay, "ssys_source_id": source_id});
    return this.http.post(this._url,body,
      options
      )
      .map(this.setOffer)
      .catch(this.handleError)
  }

  getOfferViewAll(offer_set_id: number) {

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', 'Bearer ' + localStorage.getItem('id_token'));
    let options = new RequestOptions({ headers: headers });

    return this.http.put(this._url+'viewall/'+offer_set_id,      {},
      options
      )
      .map(this.setOffer)
      .catch(this.handleError)

  }

  public setOfferWithheld(offer_set_id: number) {

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', 'Bearer ' + localStorage.getItem('id_token'));
    let options = new RequestOptions({ headers: headers });

    return this.http.put(this._url + 'withhold/' + offer_set_id ,
      {},
      options
      )
      .map(this.setOffer)
      .catch(this.handleError)
  }

  public setOfferAccepted(offer_id: number, offer_set_id: number, promo_id: string) {

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', 'Bearer ' + localStorage.getItem('id_token'));
    let options = new RequestOptions({ headers: headers });

    let body = promo_id ? JSON.stringify({"offer_id": offer_id,"promo_id":promo_id}) : JSON.stringify({"offer_id": offer_id});
    return this.http.put(this._url + 'accept/' + offer_set_id ,body,
      options
      )
      .map(this.setOffer)
      .catch(this.handleError)
  }



  public setOfferDeclined(offer_id: number) {

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', 'Bearer ' + localStorage.getItem('id_token'));
    let options = new RequestOptions({ headers: headers });

    let body = JSON.stringify({"offer_id": offer_id});
    return this.http.put(this._url + 'decline/' + offer_id ,body,
      options
      )
      .map(this.setOffer)
      .catch(this.handleError)


  }

  private setOffer(data){
    return data.json()
  }

  private handleError (error: any) {
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg); // log to console instead
    return Observable.throw(errMsg);
  }


}
