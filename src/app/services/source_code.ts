import {Injectable, NgZone} from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { config } from '../config';
import { SourceCode } from '../interfaces/sourceCode';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class SourceCodeService {

  private _baseUrl = config.getEnvironmentVariable('endPoint');

  constructor(public http: Http) {
    
  }

  getSourceCodes(id: number) {

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', 'Bearer ' + localStorage.getItem('id_token'))
    let options = new RequestOptions({ headers: headers });


    return this.http.get(this._baseUrl + 'hotel/'+id + '/source_code',
      options
      )
      .map(this.setSourceCode)
      .catch(this.handleError)

  }

  private setSourceCode(data){
      let tempClasses: Array<SourceCode> = [];

      for (var datum of data.json()) {
        
          let tempClass = new SourceCode();

          tempClass.fillFromObject(datum);
          tempClasses.push(tempClass);
      }
      return tempClasses;
  }

/*
    get availableHotels(): Array<Hotel> {
        return this._availableHotels;
    }
    set availableHotels(newHotelList: Array<Hotel>) {
        this._availableHotels = newHotelList;
    }
*/
  private handleError (error: any) {
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg); // log to console instead
    return Observable.throw(errMsg);
  }

}

