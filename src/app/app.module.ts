///<reference path="components/admin/admin_reporting.ts"/>
import { NgModule }      from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import {AppComponent}   from './app.component';
import {HotelComponent}   from './components/hotels';
import {OfferGenericComponent}   from './components/offer_generic';
import {GenericComponent}   from './components/generic';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {Shared}   from './services/shared';
import {LoginComponent}   from './components/login';
import {ConfirmationComponent}   from './components/confirmation';
import {AdminInventoryComponent}   from './components/admin/admin_inventory';
import {AdminAllocationComponent}   from './components/admin/admin_allocation';
import {AdminAllocationDefaultComponent}   from './components/admin/admin_allocation_default';
import {AdminInventoryAlertsComponent}   from './components/admin/admin_inventory_alerts';
import {AdminPricingDefaultComponent}   from './components/admin/admin_pricing_default';
import {AdminPricingBarComponent}   from './components/admin/admin_pricing_bar';
import {AdminPricingDiscComponent}   from './components/admin/admin_pricing_disc';
import {AdminReportingComponent} from './components/admin/admin_reporting';
import {AdminComponent}   from './components/admin/admin';
import {AdminGeneralComponent}   from './components/admin/admin_general';
import { AdminDefaultDowComponent } from './components/admin/admin_default_dow';
import { AdminDefaultLosComponent } from './components/admin/admin_default_los';
import { AdminDefaultToaComponent } from './components/admin/admin_default_toa';
import { AdminDefaultAduComponent } from './components/admin/admin_default_adu';
import { AdminDefaultLimitsComponent } from './components/admin/admin_default_limits';
import { AdminDefaultBarComponent } from './components/admin/admin_default_bar';
import {ConfirmationVerifyComponent}   from './components/confirmation_verify';
import {TitleCasePipe}  from './pipes/title-case.pipe';
import {AuthGuard}  from './services/auth_guard_service';
import {AuthService}  from './services/auth_service';
import { AdminPromoComponent } from './components/admin/admin_promo';

import { ProfileComponent }   from './components/profile';
import { RecoveryComponent }   from './components/recovery';

import { OnlyNumberDirective } from './directives/only_number';
import { EqualValidator } from './directives/equal_validator';


import {HttpModule} from '@angular/http';
import {
    routing,
    appRoutingProviders
} from './app.routing';
import {Ng2Bs3ModalModule} from 'ng2-bs3-modal/ng2-bs3-modal';
import {UiSwitchModule} from 'angular2-ui-switch';
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';
////import {NgxDatatableModule} from '@swimlane/ngx-datatableZZZZZZ';

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        routing,
        NgbModule.forRoot(),
        Ng2Bs3ModalModule,
        UiSwitchModule,
        NgxMyDatePickerModule
        //NgxMyDatePickerModule.forRoot()
    ],
    declarations: [LoginComponent,
        AppComponent,
        HotelComponent,
        OfferGenericComponent,
        GenericComponent,
        ConfirmationComponent,
        TitleCasePipe,
        ConfirmationVerifyComponent,
        AdminInventoryComponent,
        AdminInventoryAlertsComponent,
        AdminAllocationComponent,
        AdminAllocationDefaultComponent,
        AdminPricingDefaultComponent,
        AdminComponent,
        AdminGeneralComponent,
        AdminPricingBarComponent,
        AdminPricingDiscComponent,
        AdminReportingComponent,
        AdminDefaultDowComponent, AdminDefaultLosComponent, AdminDefaultToaComponent, AdminDefaultAduComponent, AdminDefaultLimitsComponent, AdminDefaultBarComponent,
        AdminPromoComponent,
        OnlyNumberDirective, EqualValidator,
        ProfileComponent, RecoveryComponent],

    bootstrap: [AppComponent],
    providers: [Shared, AuthGuard, AuthService]
})
export class AppModule {
}

