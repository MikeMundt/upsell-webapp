import {Component, OnInit, OnChanges} from '@angular/core';

import {Shared} from '../../services/shared'
import {Inventory} from '../../services/inventory'

import {AdminRoomInventory} from '../../interfaces/adminRoomInventory';

import {inventoryLevel} from '../../helpers';

declare var module: any; //needed for moduleId
declare var $: any;

@Component({
    moduleId: module.id,
    selector: 'luxe-admin-inventory',
    templateUrl: "../../templates/admin/admin_inventory.template.html",
    providers: [Inventory]
})
export class AdminInventoryComponent implements OnInit, OnChanges {
    roomInventory: AdminRoomInventory[];
    inventoryDates: Date[];
    roomCategories: String[];
    columnData: any;
    rowData: any;
    errorMessage: any;
    cells: any;
    page: number;

    constructor(private shared: Shared, private inventory: Inventory) {
        this.shared = shared;
        this.inventory = inventory;
        this.cells = [
            {value: 'L', color: 'green'},
            {value: 'VL', color: 'orange'},
            {value: 'X', color: 'red'}
        ]
        this.page = 0;
    }

    ngOnInit() {
        let that = this;
        this.inventory.getInventory(this.shared.selectedHotel, this.page).subscribe(
            inventory => {
                this.columnData = inventory;
                inventory.map(function (row) {
                    row.dates.map(function (col) {
                        that.cells.forEach(function (el, idx) {
                            if (col.avail_cd == el.value) {
                                col.idx = ++idx;
                                col.color = el.color;
                            }
                            if (!col.color)
                                col.color = ""
                        });
                    });
                });
                this.rowData = inventory[0].dates.map(function (x) {
                    //return x.dt_stay.split('-')[1].replace("0", "") + "/" + x.dt_stay.split('-')[2];
                    let d = new Date(x.dt_stay);
                    let f = (d.getMonth() + 1) + "/" + (d.getDate() + 1);
                    return f;
                });
            },
            error => this.errorMessage = <any>error);
    }

    navPaging(direction: string) {
        switch (direction)
        {
            case 'up':
                this.page++;
                break;
            case 'down':
                this.page--;
                break;
        }

        if (this.page < 0) this.page = 0;

        let that = this;
        this.inventory.getInventory(this.shared.selectedHotel, this.page).subscribe(
            inventory => {
                this.columnData = inventory;
                inventory.map(function (row) {
                    row.dates.map(function (col) {
                        that.cells.forEach(function (el, idx) {
                            if (col.avail_cd == el.value) {
                                col.idx = ++idx;
                                col.color = el.color;
                            }
                            if (!col.color)
                                col.color = ""
                        });
                    });
                });
                this.rowData = inventory[0].dates.map(function (x) {
                    //return x.dt_stay.split('-')[1].replace("0", "") + "/" + x.dt_stay.split('-')[2];
                    let d = new Date(x.dt_stay);
                    let f = (d.getMonth() + 1) + "/" + (d.getDate() + 1);
                    return f;
                });
            },
            error => this.errorMessage = <any>error);
    }

    clicked(event, room: any, cell: any) {
        if (!cell.idx) cell.idx = 0;

        if (cell.idx >= this.cells.length) {
            event.target.innerHTML = "";
            cell.idx = 0;
            this.inventory.updateInventory(this.shared.selectedHotel, room.room_category_id, cell.dt_stay, "").subscribe(
                inventory => {
                    console.log('updated');
                });
        }
        else {
            let cellDisplay = this.cells[cell.idx];
            event.target.innerHTML = cellDisplay.value;
            event.target.style.color = cellDisplay.color;
            cell.idx++;

            this.inventory.updateInventory(this.shared.selectedHotel, room.room_category_id, cell.dt_stay, cellDisplay.value).subscribe(
                inventory => {
                    console.log('updated');
                });
        }
        event.preventDefault();

    }


    ngOnChanges() {
        for (let j of this.roomInventory) {
            this.inventoryDates.push(j.inventory_date)
        }

        for (let j of this.roomInventory) {
            this.roomCategories.push(j.room_category_name)
        }

        this.inventoryDates = this.inventoryDates.sort(function (a, b) {
            a = new Date(a);
            b = new Date(b);
            return a > b ? -1 : a < b ? 1 : 0;
        });

        this.roomCategories = this.roomCategories.sort();

        this.inventoryDates.filter(function (value, index, self) {
            return self.indexOf(value) === index;
        });
        this.roomCategories.filter(function (value, index, self) {
            return self.indexOf(value) === index;
        });

        this.columnData = {"prop": "id"};

        for (let j of this.inventoryDates) {
            this.columnData.push({"name": this.formatDate(j)});
        }


    }

    formatDate(unformattedDate: Date) {
        let month = '' + (unformattedDate.getMonth() + 1);
        let day = '' + unformattedDate.getDate();
        let year = unformattedDate.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join('-');
    }
}
