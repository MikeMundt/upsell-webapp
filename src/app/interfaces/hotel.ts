import { Serializable } from './serializable';

export class Hotel extends Serializable {
    hotel_id: string;
    hotel_name: string;

}
