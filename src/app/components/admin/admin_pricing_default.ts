import {Component, OnInit, OnChanges} from '@angular/core';

import {Shared} from '../../services/shared'
import {Pricing} from '../../services/pricing'

import {AdminRoomInventory} from '../../interfaces/adminRoomInventory';

import {inventoryLevel} from '../../helpers';

declare var module: any; //needed for moduleId
declare var $: any;

@Component({
    moduleId: module.id,
    selector: 'luxe-admin-pricing-default',
    templateUrl: "../../templates/admin/admin_pricing_default.template.html",
    providers: [Pricing]
})
export class AdminPricingDefaultComponent implements OnInit, OnChanges {
    roomInventory: AdminRoomInventory[];
    inventoryDates: Date[];
    roomCategories: String[];
    columnData: any;
    rowData: any;
    errorMessage: any;
    cells: any;
    dates: any;

    constructor(private shared: Shared, private pricing: Pricing) {
         this.shared = shared;
         this.pricing = pricing;
         this.cells = [
           { value: 'L', color: 'green'},
           { value: 'VL', color: 'orange'}, 
           { value:'X', color:'red' }
         ]
         this.dates = [
          "SUN","MON","TUE","WED",
          "THU","FRI","SAT"
         ]
    }

    ngOnInit () {
          var that = this;
      this.pricing.getDefault(this.shared.selectedHotel).subscribe(
          pricing => {
            this.rowData = pricing[0].default_discounts;
            this.columnData = pricing
            this.columnData.map(function(row) {
              row.default_discounts.map(function(date){
                date.color = "rgb(90,90,90)";
                
              });

            });
          },
          error =>  this.errorMessage = <any>error);
    }

    checkBackground(date:any)
    {
      return date.pricing_def_ovrd ? 'rgb(38, 38, 38)': 'transparent' ;
    }

    highlight(event:any) {

      var range = document.createRange();
      range.selectNodeContents(event.currentTarget);
      var sel = window.getSelection();
      sel.removeAllRanges();
      sel.addRange(range);
      event.target.focus();
    }

    clicked(event, room: any,  date:any, type:any) {
      //event.target.innerHTML = "";
      let new_value = event.target.outerText.replace("%","") / 100

      if (new_value == NaN || new_value == date[type])
      {
        return false;
      }
      console.log(room);
      console.log(date);
      console.log(type);

      event.currentTarget.style.color = "white";
      event.currentTarget.style.background = "rgb(38,38,38)";
      event.preventDefault();
      date[type] = new_value;

      this.pricing.updateDefaultPricing(this.shared.selectedHotel, room.room_category_id, date.dow, date.disc_min, date.disc_max, date.disc_dflt, date.price_default).subscribe(
        pricing => {
          console.log(pricing)
        },
      error =>  this.errorMessage = <any>error);

    }



    checkEnter(event, room: any,  date:any, type:any) 
    {
      console.log(event)
      if (event.keyCode === 13)
      {
        event.target.blur();
        event.preventDefault();
      }

    }


    ngOnChanges () {


    }

    formatDate(unformattedDate: Date){
        let month = '' + (unformattedDate.getMonth() + 1);
        let day = '' + unformattedDate.getDate();
        let year = unformattedDate.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join('-');
    }
}
