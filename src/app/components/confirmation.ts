import { Component, OnInit, Injectable } from '@angular/core';

import {
  CanActivate,
  Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  ActivatedRoute,
  NavigationEnd
  
} from '@angular/router';

import { Shared }   from '../services/shared';
import { ConfirmationService  } from '../services/confirmation'

declare var module: any; //needed for moduleId
@Component({
    moduleId: module.id,
    selector: 'luxe-radial-upsell',
    templateUrl: '../templates/confirmation.html',
    providers: [ConfirmationService]
})
export class ConfirmationComponent implements OnInit {
    confirmationId: string;
    errorMessage: string;
    constructor(private router: Router, private shared: Shared, private confirmationService: ConfirmationService) {
      this.router = router;
      this.shared = shared;
    }

    onKey(confirmationId: string) {
      this.confirmationId = confirmationId
    }


    ngOnInit() { 


    }

    confirmation_clicked(event) {
        this.errorMessage = '';
      event.preventDefault();

        this.confirmationService.getConfirmation(this.confirmationId, this.shared.selectedHotel).subscribe(
            confirmation => this.continue(),
            error => this.errorMessage = "Confirmation does not exist. Please try again!");
    }

    continue() {
        this.shared.last_url = this.router.url;
        this.router.navigate(["/confirmation", this.shared.selectedHotel,this.confirmationId])
    }
}
