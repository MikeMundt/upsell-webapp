import { Serializable } from './serializable';

export class SourceCode extends Serializable {
    source_id: string;
    source_code: string;
    source_description: string;
    group_code: string;
    group_description: string;
}
