import { Component, OnInit, Injectable, ViewChild, NgZone } from '@angular/core';
import { Account } from '../services/account'
import { config } from '../config'
import {
    CanActivate,
    Router,
    ActivatedRouteSnapshot,
    RouterStateSnapshot,
    ActivatedRoute
} from '@angular/router';

import { Shared }   from '../services/shared';

declare var module: any; //needed for moduleId
@Component({
    moduleId: module.id,
    selector: 'profile',
    templateUrl: '../templates/profile.template.html',
    providers: [Account]
})
export class ProfileComponent implements OnInit {
    errorMessage: string = '';
    currentPassword: string;
    newPassword: string;
    confirmPassword: string;
    successful: boolean = false;
    constructor(private accountService : Account, private router: Router, private shared: Shared, private _ngZone: NgZone) {
        this.accountService = accountService;
        this.router = router;
        this.shared = shared;
    }

    ngOnInit() {

    }

    savePassword() {
        this.accountService.updatePassword(this.currentPassword, this.newPassword).subscribe( () => {
                this.successful = true;
            },
            error =>  {
                this.errorMessage = "Unable to update your password."
            })
    }

    goHome() {
        this.shared.reset();
        this.shared.show_res_conf = false;
        this.router.navigate(["generic", this.shared.selectedHotel])
    }

}
