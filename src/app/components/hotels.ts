import {Component, OnInit, Injectable} from '@angular/core';
import {Hotels} from '../services/hotels'
import {Hotel} from '../interfaces/hotel'
import {config} from '../config'
import {
    CanActivate,
    Router,
    ActivatedRouteSnapshot,
    RouterStateSnapshot,
    ActivatedRoute,
    NavigationEnd

} from '@angular/router';

import {Shared}   from '../services/shared';

declare var module: any; //needed for moduleId
@Component({
    moduleId: module.id,
    selector: 'luxe-radial-upsell',
    templateUrl: '../templates/hotel.template.html',
    providers: [Hotels]
})
export class HotelComponent implements OnInit {
    errorMessage: string;
    hotels: Hotel[];

    constructor(private hotelService: Hotels, private router: Router, private shared: Shared) {
        this.hotelService = hotelService;
        this.router = router;
        this.shared = shared;
        this.shared.show_res_conf = false;
    }

    ngOnInit() {
        this.hotelService.getHotels().subscribe(
            hotels => {
                this.hotels = hotels;
                this.shared.availableHotels = hotels;
                this.shared.selectedHotel = this.hotels[0].hotel_id
            },
            error => this.errorMessage = <any>error);
    }


    clicked(hotelId: string) {
        this.shared.selectedHotel = hotelId;
        this.shared.sourceCodes = [];
        this.router.navigate(["/generic", hotelId]);
    }
}
