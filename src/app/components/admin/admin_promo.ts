import {Component, OnInit, NgZone, ViewChild, ChangeDetectorRef, ApplicationRef } from '@angular/core';
import {INgxMyDpOptions, IMyDateModel} from 'ngx-mydatepicker';

import {Shared} from '../../services/shared'
import {AdminPromo} from '../../services/admin_promo'
import { ModalComponent } from 'ng2-bs3-modal/ng2-bs3-modal';

declare var module: any; //needed for moduleId
declare var $: any;

@Component({
    moduleId: module.id,
    selector: 'luxe-admin-promo',
    templateUrl: "../../templates/admin/admin_promo.template.html",
    providers: [AdminPromo]
})
export class AdminPromoComponent implements OnInit {
    promoList: any;
    errorMessage: any;
    promo: any = {};
    promoClone: any = {};
    isLoading: boolean = false;
    dateModel: any;
    newPromoCode: string;

    @ViewChild('modal')
    modal: ModalComponent;
    @ViewChild('createModal')
    createModal: ModalComponent;
    modal_body: string;

    constructor(private shared: Shared, private promo_service: AdminPromo, public lc: NgZone,private ref: ChangeDetectorRef, private appRef: ApplicationRef) {
        this.shared = shared;
        this.promo_service = promo_service;
        this.lc = lc;
    }


    ngOnInit () {
        this.getPromoList();

        let that = this;
        $(".promo-array").on("select2:select", function (e) { that.selectPromo(e.params.data); });
    }

    sortPromoList() {
        this.promoList.sort( function(d1, d2) {
            if ( d1.text < d2.text ){
                return -1;
            }else if( d1.text > d2.text ){
                return 1;
            }else{
                return 0;
            }
        });

        $(".promo-array").select2({
            data: this.promoList
        });
    }

    getPromoList() {
        this.promo_service.getPromoList(this.shared.selectedHotel).subscribe(
            data => {
                this.promoList = data.map(function (x) {
                    let obj = {};
                    obj = { id: x.promo_id, text: x.promo_cd };
                    return obj;
                });
                this.sortPromoList();

                if (this.promoList) {
                    this.getPromo(this.promoList[0].id);
                }
            },
            error => this.errorMessage = <any>error);
    }

    getPromo(promo_id: string) {
        this.promo_service.getPromo(this.shared.selectedHotel, promo_id).subscribe(
            data => {
                this.promo = data[0];
                this.promoClone = Object.assign({}, this.promo);

                this.dateModel = null;
                if (this.promo.dt_expire) {
                    let dt = new Date(this.promo.dt_expire);
                    this.dateModel = {date: {year: dt.getFullYear(), month: dt.getMonth() + 1, day: dt.getDate() + 1}};
                }
                //this.dateModel = {date: {year: 2017, month: 10, day: 5}}
            },
            error => this.errorMessage = <any>error);
    }

    refreshPromo() {
        let clone = Object.assign({}, this.promo);
        this.promo = {};
        this.ref.detectChanges();
        this.lc.run(() => { this.promo = Object.assign({}, clone);
                            this.promoClone = Object.assign({}, this.promo);
        });

    }

    openModal(){
        this.newPromoCode = '';
        this.createModal.open();
    }

    createPromo(){
        let that = this;
        this.promo_service.createPromo(this.shared.selectedHotel, this.newPromoCode).subscribe(
            response => {
                this.createModal.close();

                if (response.success) {
                    this.modal_body = 'Promo Created';
                    this.promo = response.data[0];
                    this.promoClone = Object.assign({}, this.promo);

                    this.promoList.push({ id: this.promo.promo_id, text: this.promo.promo_cd });
                    this.sortPromoList();
                    $(".promo-array").select2().val(this.promo.promo_id).trigger("change");
                } else {
                    this.modal_body = 'Promo Code already exists. Please select an alternate.'
                }
                this.modal.open();
                setTimeout(function(){
                    that.modal.close();
                },2000)
            },
            error =>  this.errorMessage = <any>error);
    }

    selectPromo(promo: any) {
        this.getPromo(promo.id);
    }

    clicked(event, category: any, field: string) {
        if (!event) event = window.event;
        let tar = event.target || event.srcElement;
        let new_value = tar.innerText;
        tar.innerText = '...';
        let curField = field == 'price' ? category.price : category.factor;
        if (isNaN(new_value) || Number(new_value) < 0) {
            this.refreshPromo();
            return false;
        }

        if (this.isLoading) return false;
        this.isLoading = true;
        console.log(new_value);
        if (!isNaN(new_value) && isFinite(new_value) && new_value != '') {
            category.flg_include_upsell = true;
            if (field == 'price') {
                category.price = new_value;
                category.factor = null;
            } else {
                category.factor = new_value;
                category.price = null;
            }
            this.promo_service.updatePromoRoomCategory(this.shared.selectedHotel, this.promo.promo_id, category.promo_room_category_id, category).subscribe(
                d => {
                    this.refreshPromo();
                    this.isLoading = false;
                },
                error => this.errorMessage = <any>error);


        }
        else {
            category.flg_include_upsell = false;
            if (field == 'price') {
                category.price = null;
            } else {
                category.factor = null;
            }
            this.promo_service.updatePromoRoomCategory(this.shared.selectedHotel, this.promo.promo_id, category.promo_room_category_id, category).subscribe(
                d => {
                    this.refreshPromo();
                    this.isLoading = false;
                },
                error => this.errorMessage = <any>error);

        }
    }

    saveCategory(category: any, type: string) {
        switch (type) {
            case 'base':
                category.flg_include_base = !category.flg_include_base;
                break;
            case 'upsell':
                category.flg_include_upsell = !category.flg_include_upsell;
                break;
        }
        this.promo_service.updatePromoRoomCategory(this.shared.selectedHotel, this.promo.promo_id, category.promo_room_category_id, category).subscribe(
            data => {
            },
            error => this.errorMessage = <any>error);
    }

    save() {
        if (this.isLoading)
            return;

        this.isLoading = true;
        let that = this;
        setTimeout(function(){
            that.promo_service.updatePromo(that.shared.selectedHotel, that.promo.promo_id, that.promo).subscribe(
                data => {
                    that.isLoading = false;
                },
                error => that.errorMessage = <any>error);
        },1000)
    }

    setActive() {
        this.promo.flg_active = !this.promo.flg_active;
        this.save();
    }

    onDateChanged(event: IMyDateModel): void {
        if (event.jsdate) {
            this.promo.dt_expire = event.date.month + '/' + event.date.day + '/' + event.date.year;
        } else {
            this.promo.dt_expire = null;
        }
        this.save();
    }

    promoCode() {
        this.promoClone.promo_cd_timeout = null;
        if(this.promoClone.promo_cd_timeout){ //if there is already a timeout in process cancel it
            window.clearTimeout(this.promoClone.promo_cd_timeout);
        }
        this.promoClone.promo_cd_timeout = window.setTimeout(() => {
            this.promoClone.promo_cd_timeout = null;
            this.lc.run(() => this.promo.promo_cd = this.promoClone.promo_cd);
            this.save();
        },3000);
    }
    promoMaxOffered() {
        this.promoClone.max_offered_timeout = null;
        if(this.promoClone.pmax_offered_timeout){ //if there is already a timeout in process cancel it
            window.clearTimeout(this.promoClone.max_offered_timeout);
        }
        this.promoClone.max_offered_timeout = window.setTimeout(() => {
            this.promoClone.max_offered_timeout = null;
            this.lc.run(() => this.promo.max_offered = this.promoClone.max_offered);
            this.save();
        },3000);
    }
    promoMaxRedemption() {
        this.promoClone.max_redemptions_timeout = null;
        if(this.promoClone.max_redemptions_timeout){ //if there is already a timeout in process cancel it
            window.clearTimeout(this.promoClone.max_redemptions_timeout);
        }
        this.promoClone.max_redemptions_timeout = window.setTimeout(() => {
            this.promoClone.max_redemptions_timeout = null;
            this.lc.run(() => this.promo.max_redemptions = this.promoClone.max_redemptions);
            this.save();
        },3000);
    }
    promoCustomerName() {
        this.promoClone.customer_name_timeout = null;
        if(this.promoClone.customer_name_timeout){ //if there is already a timeout in process cancel it
            window.clearTimeout(this.promoClone.customer_name_timeout);
        }
        this.promoClone.customer_name_timeout = window.setTimeout(() => {
            this.promoClone.customer_name_timeout = null;
            this.lc.run(() => this.promo.customer_name = this.promoClone.customer_name);
            this.save();
        },3000);
    }

    highlight(event: any) {
        event.preventDefault();
        var range = document.createRange();
        range.selectNodeContents(event.currentTarget);
        var sel = window.getSelection();
        sel.removeAllRanges();
        sel.addRange(range);
        event.currentTarget.focus();
    }
}