import {Component, OnInit, OnChanges } from '@angular/core';

import {Shared} from '../../services/shared'
import {Pricing} from '../../services/pricing'


import {inventoryLevel} from '../../helpers';

declare var module: any; //needed for moduleId
declare var $: any;

@Component({
    moduleId: module.id,
    selector: 'luxe-admin-inventory',
    templateUrl: "../../templates/admin/admin_bar.template.html",
    providers: [Pricing]
})
export class AdminPricingBarComponent implements OnInit, OnChanges {
    barDates: Date[];
    barInventory: any;
    roomCategories: String[];
    columnData: any;
    rowData: any;
    errorMessage: any;
    color: String;
    pF = parseFloat;
    iF = isFinite;
    iN = isNaN;
    page: number;

    constructor(private shared: Shared, private pricing: Pricing) {
        this.color = "rgb(90,90,90)";
        this.shared = shared;
        this.pricing = pricing;
        this.page = 0;
    }

    checkPrice(date: any) {
        return date.price_bar_ovrd ? 'white' : "rgb(90,90,90)";
    }

    checkBackground(date: any) {
        return date.price_bar_ovrd ? 'rgb(38, 38, 38)' : 'transparent';
    }

    ngOnInit() {
        let that = this;
        this.pricing.getBar(this.shared.selectedHotel, this.page).subscribe(
            pricing => {
                this.columnData = pricing;
                pricing.map(function (row) {
                    //row.daily_values.map(function(col) {
                    //});
                });
                this.rowData = pricing[0].daily_values.map(function (x) {
                    //return x.dt_stay.split('-')[1].replace("0", "") + "/" + x.dt_stay.split('-')[2];
                    let d = new Date(x.dt_stay);
                    let f = (d.getMonth() + 1) + "/" + (d.getDate() + 1);
                    return f;
                });
            },
            error => this.errorMessage = <any>error);
    }

    checkEnter(event, room: any, date: any, type: any) {
        console.log(event);
        if (event.keyCode === 13) {
            event.target.blur();
            event.preventDefault();
        }

    }

    getDisplayValue(date: any): number {
        let value = 0;

        if (date.price_bar_ovrd)
            value = Number(date.price_bar_ovrd);

        if (!date.price_bar_ovrd)
            value = date.price_bar;

        return value;
    }

    navPaging(direction: string) {
        switch (direction)
        {
            case 'up':
                this.page++;
                break;
            case 'down':
                this.page--;
                break;
        }

        if (this.page < 0) this.page = 0;

        this.pricing.getBar(this.shared.selectedHotel, this.page).subscribe(
            pricing => {
                this.columnData = pricing;
                pricing.map(function (row) {
                    //row.daily_values.map(function(col) {
                    //});
                });
                this.rowData = pricing[0].daily_values.map(function (x) {
                    //return x.dt_stay.split('-')[1].replace("0", "") + "/" + x.dt_stay.split('-')[2];
                    let d = new Date(x.dt_stay);
                    let f = (d.getMonth() + 1) + "/" + (d.getDate() + 1);
                    return f;
                });
            },
            error => this.errorMessage = <any>error);
    }

    clicked(event, room: any, date: any) {
        if (!event) event = window.event;
        let tar = event.target || event.srcElement;
        let new_value = tar.innerText.replace("%", "").replace('$', "");

        if (isNaN(new_value) || parseInt(new_value) === parseInt(date.price_bar)) {
            tar.innerText = date.price_bar_ovrd ? '$' + parseInt(date.price_bar_ovrd) : '$' + parseInt(date.price_bar);
            return false;
        }

        //event.currentTarget.style.color = "white";
        //event.preventDefault();

        //console.log(!isNaN(parseFloat(new_value)) && isFinite(new_value));
        if (!isNaN(parseFloat(new_value)) && isFinite(new_value)) {
            this.pricing.updateBarPricing(this.shared.selectedHotel, room.room_category_id, date.dt_stay, new_value).subscribe(
                pricing => {
                    date.price_bar_ovrd = new_value;
                    tar.innerText = '$' + parseInt(new_value);
                },
                error => this.errorMessage = <any>error);
        }
        else {
            this.pricing.removeBarPricing(this.shared.selectedHotel, room.room_category_id, date.dt_stay).subscribe(
                pricing => {
                    date.price_bar_ovrd = null;
                    tar.innerText = '$' + parseInt(date.price_bar);
                },
                error => this.errorMessage = <any>error);
        }

    }


    highlight(event: any) {
        event.preventDefault();
        var range = document.createRange();
        range.selectNodeContents(event.currentTarget);
        var sel = window.getSelection();
        sel.removeAllRanges();
        sel.addRange(range);
        event.currentTarget.focus();
    }


    ngOnChanges() {
        for (let j of this.barInventory) {
            this.barDates.push(j.inventory_date);
        }

        for (let j of this.barInventory) {
            this.roomCategories.push(j.room_category_name);
        }

        this.barDates = this.barDates.sort(function (a, b) {
            a = new Date(a);
            b = new Date(b);
            return a > b ? -1 : a < b ? 1 : 0;
        });

        this.roomCategories = this.roomCategories.sort();

        this.barDates.filter(function (value, index, self) {
            return self.indexOf(value) === index;
        });
        this.roomCategories.filter(function (value, index, self) {
            return self.indexOf(value) === index;
        });

        this.columnData = {"prop": "id"};

        for (let j of this.barDates) {
            this.columnData.push({"name": this.formatDate(j)});
        }


    }

    formatDate(unformattedDate: Date) {
        let month = '' + (unformattedDate.getMonth() + 1);
        let day = '' + unformattedDate.getDate();
        let year = unformattedDate.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join('-');
    }
}
