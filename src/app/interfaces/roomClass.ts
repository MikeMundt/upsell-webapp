import { Serializable } from './serializable';
import { RoomType } from './roomType';

export class RoomClass extends Serializable {
    class: string;
    room_types: RoomType[];
}
