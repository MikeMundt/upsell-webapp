import { Component, OnInit, AfterViewInit,ViewChild, HostListener } from '@angular/core';

import { Shared }   from '../services/shared';
import { Router, ActivatedRoute } from '@angular/router';
import { ConfirmationService  } from '../services/confirmation'

declare var module: any; //needed for moduleId
declare var $: any;
@Component({
    moduleId: module.id,
    selector: 'luxe-radial',
    templateUrl: '../templates/confirmation_verify.template.html',
    providers: [ConfirmationService]

})
export class ConfirmationVerifyComponent implements OnInit{

  errorMessage:string;
  confirmation:any;
  confirmation_id:string;
  hotel_id:number;

  showPromo: boolean;

  @HostListener('document:keypress', ['$event'])
  handleKeyboardEvent(event: any) {
    if (event.keycode == 13)
      this.navigate();
  }

  constructor(private confirmationService: ConfirmationService, private shared: Shared, private route: ActivatedRoute,  private router: Router) { 
    this.confirmation = {}
    
  }

  ngOnInit() {
    this.shared.promoCode = '';
    this.confirmation_id = this.route.snapshot.params['confirmation'];
    this.hotel_id = this.route.snapshot.params['id'];
    
    this.confirmationService.getConfirmation(this.confirmation_id, this.hotel_id).subscribe(
      confirmation => this.parseConfirmation(confirmation),
      error => this.errorMessage = <any>error);

  }

  parseConfirmation(confirmation) {
    this.confirmation = confirmation;
  }

  cancel(e) {
    e.preventDefault();
    this.router.navigate(['confirm']);
    
  }

  gotoOffer(e) {
    e.preventDefault();
    this.router.navigate(['offer',this.hotel_id,this.confirmation_id]);
    
  }


  navigate() {
    if (this.shared.usePromo && !this.showPromo) {
      this.showPromo = true;
      setTimeout(function(){
        document.getElementById("txtPromoCode").focus();
      }, 200);

    } else {
      this.getOffer();
    }

  }

  getOffer() {
    if (this.shared.usePromo) {
      this.shared.params['promo_cd'] = this.shared.promoCode.toUpperCase();
    }

    this.shared.params['base_room_category_id'] = this.confirmation.base_room_category_id;
    this.shared.params['los'] = this.confirmation.los;
    //this.shared.params['reservation_id'] = this.confirmation.reservation_id;
    this.shared.params['conf_num'] = this.confirmation_id.toUpperCase();
    this.shared.last_url = this.router.url;
    this.router.navigate(['offer_generic', this.shared.selectedHotel]);
  }

}
