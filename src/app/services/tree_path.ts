import {Injectable, NgZone} from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { config } from '../config';


import { Observable } from 'rxjs/Rx';


@Injectable()
export class TreePath {

  private _url = config.getEnvironmentVariable('endPoint');

  constructor(public http: Http) {
  }

   getTreePath(hotel_id) {

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', 'Bearer ' + localStorage.getItem('id_token'))
    let options = new RequestOptions({ headers: headers });


    return this.http.get(this._url+'hotel/' + hotel_id + '/tree_path',
      options
      )
      .map(this.parseTreePath)
      .catch(this.handleError)

  }

    getQuickClick(hotel_id) {

        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('id_token'))
        let options = new RequestOptions({ headers: headers });


        return this.http.get(this._url+'hotel/' + hotel_id + '/room_category',
            options
        )
            .map(this.parseQuickClick)
            .catch(this.handleError)

    }
  
  private parseTreePath(data)
  {
    return data.json();
  }

    private parseQuickClick(data)
    {
        let d = data.json();

        var items: any[] = [];
        for(var i = 1; i <= 7; i++){
            items.push({value_display_name: i, value_desc: i + ' Nights'});
        }
        items.push({value_display_name: 10, value_desc: '10 Nights'});
        items.push({value_display_name: 14, value_desc: '14 Nights'});
        items.push({value_display_name: 21, value_desc: '21 Nights'});

        d.forEach(function(x) {
            x.los = Object.assign([], items);
        });

       /* d.sort( function(d1, d2) {
            if ( d1.room_category_name < d2.room_category_name ){
                return -1;
            }else if( d1.room_category_name > d2.room_category_name ){
                return 1;
            }else{
                return 0;
            }
        });*/

        return d;
    }

  private handleError (error: any) {
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg); // log to console instead
    return Observable.throw(errMsg);
  }

}
