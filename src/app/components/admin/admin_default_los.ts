import {Component, OnInit, OnChanges} from '@angular/core';

import {Shared} from '../../services/shared'
import {AdminDefault} from '../../services/admin_default'


declare var module: any; //needed for moduleId
declare var $: any;

@Component({
    moduleId: module.id,
    selector: 'luxe-admin-default-los',
    templateUrl: "../../templates/admin/admin_default_los.template.html",
    providers: [AdminDefault]
})
export class AdminDefaultLosComponent implements OnInit {
    data: any;
    errorMessage: any;

    constructor(private shared: Shared, private adminDefault: AdminDefault) {
        this.shared = shared;
        this.adminDefault = adminDefault;
    }

    ngOnInit () {
        this.adminDefault.getGeneric(this.shared.selectedHotel, 'los').subscribe(
            d => {
                this.data = d;
                this.data.forEach(function(x) {
                    x.los_desc = x.lower_range + ' nt';
                });
                this.data.sort( function(d1, d2) {
                    if ( d1.los_desc < d2.los_desc ){
                        return -1;
                    }else if( d1.los_desc > d2.los_desc ){
                        return 1;
                    }else{
                        return 0;
                    }
                });
            },
            error => this.errorMessage = <any>error);
    }

    clicked(event, val: any) {
        if (!event) event = window.event;
        let tar = event.target || event.srcElement;
        let new_value = tar.innerText;

        if (isNaN(new_value) || Number(new_value) < 0 || Number(new_value) > 2 || Number(new_value) == Number(val.factor_man)) {
            tar.innerText = parseFloat(val.factor_man).toFixed(2);
            return false;
        }

        let putObject = { factor_val: 0 };
        if (!isNaN(parseFloat(new_value)) && isFinite(new_value)) {
            putObject.factor_val = new_value;
            this.adminDefault.putGeneric(this.shared.selectedHotel, 'los', val.id, putObject).subscribe(
                d => {
                    val.factor_man = new_value;
                    tar.innerText = parseFloat(val.factor_man).toFixed(2);
                },
                error => this.errorMessage = <any>error);


        }
        else {
            putObject.factor_val = 0;
            this.adminDefault.putGeneric(this.shared.selectedHotel, 'adu', val.id, putObject).subscribe(
                d => {
                    val.factor_man = 0;
                    tar.innerText = parseFloat(val.factor_man).toFixed(2);
                },
                error => this.errorMessage = <any>error);


        }

    }

    highlight(event:any) {
        event.preventDefault();
        var range = document.createRange();
        range.selectNodeContents(event.currentTarget);
        var sel = window.getSelection();
        sel.removeAllRanges();
        sel.addRange(range);
        event.currentTarget.focus();
    }

}
