import {Component, OnInit, OnChanges} from '@angular/core';

import {Shared} from '../../services/shared'
import {AdminDefault} from '../../services/admin_default'


declare var module: any; //needed for moduleId
declare var $: any;

@Component({
    moduleId: module.id,
    selector: 'luxe-admin-default-dow',
    templateUrl: "../../templates/admin/admin_default_bar.template.html",
    providers: [AdminDefault]
})
export class AdminDefaultBarComponent implements OnInit {
    data: any;
    errorMessage: any;

    constructor(private shared: Shared, private adminDefault: AdminDefault) {
        this.shared = shared;
        this.adminDefault = adminDefault;
    }

    ngOnInit () {
        this.adminDefault.getGeneric(this.shared.selectedHotel, 'bar').subscribe(
            d => {
                this.data = d;
                this.data.sort( function(d1, d2) {
                    if ( Number(d1.display_order) < Number(d2.display_order) ){
                        return -1;
                    }else if( Number(d1.display_order) > Number(d2.display_order) ){
                        return 1;
                    }else{
                        return 0;
                    }
                });
            },
            error => this.errorMessage = <any>error);

    }

    clicked(event, val: any) {
        if (!event) event = window.event;
        let tar = event.target || event.srcElement;
        let new_value = tar.innerText.replace('$', "");

        if (isNaN(new_value) || Number(new_value) == Number(val.price_bar)) {
            tar.innerText = '$' + parseFloat(val.price_bar).toFixed(0);
            return false;
        }

        let putObject = { price_bar: 0 };
        if (!isNaN(parseFloat(new_value)) && isFinite(new_value)) {
            putObject.price_bar = new_value;
            this.adminDefault.putGeneric(this.shared.selectedHotel, 'bar', val.id, putObject).subscribe(
                d => {
                    val.price_bar = new_value;
                    tar.innerText = '$' + parseFloat(val.price_bar).toFixed(0);
                },
                error => this.errorMessage = <any>error);


        }
        else {
            putObject.price_bar = 0;
            this.adminDefault.putGeneric(this.shared.selectedHotel, 'bar', val.id, putObject).subscribe(
                d => {
                    val.price_bar = 0;
                    tar.innerText = '$' + parseFloat(val.price_bar).toFixed(0);
                },
                error => this.errorMessage = <any>error);

        }

    }

    highlight(event:any) {
        event.preventDefault();
        var range = document.createRange();
        range.selectNodeContents(event.currentTarget);
        var sel = window.getSelection();
        sel.removeAllRanges();
        sel.addRange(range);
        event.currentTarget.focus();
    }

}
