import {Injectable, NgZone} from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { config } from '../config';
import { RoomClass } from '../interfaces/roomClass';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class RoomClassService {

  private _baseUrl = config.getEnvironmentVariable('endPoint');
  private _roomClasses: Array<RoomClass> = [];

  constructor(public http: Http) {

  }

  getClasses(id: string) {

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', 'Bearer ' + localStorage.getItem('id_token'))
    let options = new RequestOptions({ headers: headers });


    return this.http.get(this._baseUrl + 'hotel/'+id + '/inventory',
      options
      )
      .map(this.setClasses)
      .catch(this.handleError)

  }

  private setClasses(data){
      let tempClasses: Array<RoomClass> = [];
      let unique = {}

      for (var datum of data.json()['data']) {

          let tempClass = new RoomClass();

          tempClass.fillFromObject(datum);
          if (!unique[tempClass['class']])
            tempClasses.push(tempClass);
          unique[tempClass['class']] = true;
      }
      return tempClasses;
  }

/*
    get availableHotels(): Array<Hotel> {
        return this._availableHotels;
    }
    set availableHotels(newHotelList: Array<Hotel>) {
        this._availableHotels = newHotelList;
    }
*/
  private handleError (error: any) {
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg); // log to console instead
    return Observable.throw(errMsg);
  }

}

