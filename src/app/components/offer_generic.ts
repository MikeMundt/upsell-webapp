import { Component, OnInit, AfterViewInit,ViewChild } from '@angular/core';
import { OfferService } from '../services/offer'
import { Hotels } from '../services/hotels'
import { Hotel } from '../interfaces/hotel';
import { config } from '../config'

import { Shared }   from '../services/shared';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalComponent } from 'ng2-bs3-modal/ng2-bs3-modal';

declare var module: any; //needed for moduleId
declare var $: any;
@Component({
    moduleId: module.id,
    selector: 'luxe-radial',
    templateUrl: '../templates/offer.template.html',
    providers: [OfferService, Hotels]

})
export class OfferGenericComponent implements OnInit,AfterViewInit {

    errorMessage: string;
    show_no_default_offers: boolean;
    offers: any[];
    details: any;
    offer_id: number;
    custom_upgrade: boolean;
    hotels: Hotel[];
    selectedHotel: Hotel;
    show_confirm: boolean;
    modal_title: string;
    show_offers: boolean;
    show_no_offers: boolean;
    @ViewChild('modal')
    modal: ModalComponent;
    @ViewChild('declinemodal')
    declinemodal: ModalComponent;
    @ViewChild('withholdmodal')
    withholdmodal: ModalComponent;
    @ViewChild('promomodal')
    promomodal: ModalComponent;
    isLoading: boolean;

    showPromoModal: boolean = false;
    showGetPromoModal: boolean = false;
    use_promo: boolean = false;
    promo: any;

    showSpeech: boolean = false;
    speechText: string;

    constructor( private shared: Shared, private route: ActivatedRoute, private offerService : OfferService, private hotelService: Hotels, private router: Router) {
      this.route = route;
      this.offerService = offerService;
      this.setDefault();
    }

    setDefault() {
        this.show_offers = false;
        this.show_no_offers = false;
        this.isLoading = false;
        this.custom_upgrade = false;
        this.promo = { exists:false };
        this.showPromoModal = false;
        this.showGetPromoModal = false;
        this.showSpeech = false;
    }

    ngOnInit() {
        console.log(this.shared);
      this.shared.show_res_conf = false;
      let confirmation_id = this.route.snapshot.params['confirmation'];
        this.isLoading = true;
      this.offerService.getOfferGeneric(this.shared.selectedHotel, this.shared.params).subscribe(
        offer =>  this.parseOffer(offer),
        error =>  this.errorMessage = <any>error);
    }

    parseOffer(offer)
    {
        this.show_no_default_offers = false;
      this.offers = offer.data.offers.filter(item => {

       if (item.flg_default_offer > 0)
       {
         item.open = true;
       }
       return item.flg_default_offer > 0

      });
      this.details = offer;
      this.offer_id = offer.id;

      if (this.offers.length)
        this.show_offers = true;
      else {
        this.custom_upgrade = true;
        this.show_no_default_offers = true;
        this.offers = offer.data.offers.filter(item => {

         if (item.flg_default_offer > 0)
         {
           item.open = true;
         }
         return item.flg_upsell_available > 0 && item.flg_downgrade == false

        });
        if (!this.offers.length){
          let that = this;
          this.show_no_offers = true;
          setTimeout(function(){
              that.shared.reset();
              //that.router.navigate([that.shared.last_url ? that.shared.last_url : '/generic/' + that.shared.selectedHotel ])
              that.router.navigate([ '/generic/' + that.shared.selectedHotel ])
          }, 1000)
        } else {
          this.show_offers = true;
        }
      }

      this.use_promo = this.shared.usePromo;
      if (this.use_promo) {

          if (offer.data.promo) {
              this.promo = offer.data.promo;
              this.promo.exists = true;
              if (this.promo.customer_name || this.promo.flg_expired || this.promo.flg_max_offers || this.promo.flg_max_redemptions || !this.promo.flg_active)
                  this.promo.errors = true;
              else
                  this.promo.errors = false;
          } else {
              this.promo = { exists: false, errors: false, promo_cd: this.shared.promoCode};
          }
          if (this.promo.errors || !this.promo.exists)
              this.showPromoModal = true;
            //this.promomodal.open();
      }

     /* if (this.shared.bread.length > 0) {
          this.currentBooking = '';

          for (var item in this.shared.bread) {
              this.currentBooking = this.currentBooking + this.shared.bread[item].display + ' ';

          }
      }*/
      this.isLoading = false;
    }

    returnTree() {
        this.showPromoModal = false;
        this.showGetPromoModal = true;

        setTimeout(function() {
            const el = <HTMLInputElement>document.getElementById('txtPromoCode');
            el.focus();
            el.select();
        }, 300);


       /* let that = this;
        setTimeout(function(){
            that.shared.reset();
            that.shared.usePromo = true;
            that.router.navigate([that.shared.last_url ? that.shared.last_url : '/generic/' + that.shared.selectedHotel ])
        }, 1000)*/
    }

    resubmitPromo() {
        this.setDefault();
        this.isLoading = true;
        this.shared.params['promo_cd'] = this.shared.promoCode.toUpperCase();
        this.offerService.getOfferGeneric(this.shared.selectedHotel,this.shared.params).subscribe(
            offer =>  this.parseOffer(offer),
            error =>  this.errorMessage = <any>error);
    }

    showComment(event, offer) {
        console.log(offer);
        let msg = 'We do have some {room_category_name} rooms available tonight. Its usually a ${bar_price} per night upgrade but I can offer it to you for only ${ofr_price} per night. Would you like to upgrade?';
        msg = msg.replace('{room_category_name}', offer.upsell_room_category_name);
        msg = msg.replace('{bar_price}', offer.bar_price);
        msg = msg.replace('{ofr_price}', offer.ofr_price);

        this.speechText = msg;
        this.showSpeech = true;

        event.preventDefault();
        event.stopPropagation();
    }

    hideModal() {
        this.showSpeech = false;
    }

    reloadWithOutPromo() {
        this.setDefault();
        this.isLoading = true;
        this.shared.usePromo = false;
        this.shared.params['promo_cd'] = null;
        this.offerService.getOfferGeneric(this.shared.selectedHotel,this.shared.params).subscribe(
            offer =>  this.parseOffer(offer),
            error =>  this.errorMessage = <any>error);
    }

    addPointer(offer)
    {
      return offer <= 0 ? 'button' : null;
    }

    toggleOffer(offer, event){
      if (!offer.open)
      {
        offer.open = true;
      } else {

        offer.open = false;
      }

    }

    selectUpsell(offer_id){
      this.offers = this.offers.filter(item => item.offer_id == offer_id);
      this.show_confirm = true;

    }

    cancelConfirm(offer_id){
      this.offers = this.details.data.offers.filter(item => item.flg_default_offer > 0);
      if (this.custom_upgrade) {
        this.offers = this.details.data.offers.filter(item => item.flg_downgrade == false && item.flg_upsell_available > 0)
      }
      this.show_confirm = false;

    }

    confirmUpsell(offer_id){

      let that = this;
      let promo_id = this.use_promo ? this.promo.promo_id : null;
      this.offerService.setOfferAccepted(offer_id,this.details.data.offer_set_id, promo_id).subscribe(
        response => {
          this.modal_title = 'Offer Processed';
          this.modal.open();
          setTimeout(function(){
            that.modal.close();
            that.shared.reset();
            that.router.navigate([ '/generic/' + that.shared.selectedHotel ])
          }, 500)
        },
        error =>  this.errorMessage = <any>error);
    }

    withholdOffer(){
      this.withholdmodal.open();
    }

    withholdConfirm(){
      let that = this;

      this.offerService.setOfferWithheld(this.details.data.offer_set_id).subscribe(
        response => {
          this.withholdmodal.close();
          this.modal_title = 'Offer Withheld';

          setTimeout(function(){
            that.modal.close();
            that.shared.reset();
            that.router.navigate([ '/generic/' + that.shared.selectedHotel ])
          },500)
        },
        error =>  this.errorMessage = <any>error);
    }

    customUpgrade(){
        this.custom_upgrade = true;
        this.offers = this.details.data.offers.filter(item => {
            item.open = false;
            return item.flg_upsell_available > 0 && item.flg_downgrade == false
        });


        this.offerService.getOfferViewAll(this.details.data.offer_set_id).subscribe(
          response => {
          },
          error =>  this.errorMessage = <any>error);
    }

    declineClick(){
      this.declinemodal.open();
    }

    declineConfirm(){
        let that = this;
        this.offerService.setOfferDeclined(this.details.data.offer_set_id).subscribe(
          response => {
            this.declinemodal.close();
            this.modal_title = 'Offer Declined';
            this.modal.open();
            setTimeout(function(){
              that.modal.close();
              that.shared.reset();
              that.router.navigate([ '/generic/' + that.shared.selectedHotel ])
            },500)

          },
          error =>  this.errorMessage = <any>error);
    }
    ngAfterViewInit() {
    }
    noAction(event: any){
      event.preventDefault();
    }
}
