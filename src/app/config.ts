export class config {

    public static getEnvironmentVariable(value) {
        var environment:string;
        var data = {};
        environment = window.location.hostname;
        switch (environment) {
            case'zzzzlocalhost':
                data = {
                    endPoint: 'http://localhost:5000/20170104/'
                    //endPoint: 'https://upsell.api.luxepricing.com/20170104/'
                };
                break;
            default:
                data = {
                    endPoint: 'https://upsell.api.luxepricing.com/20170104/'
                };
        }
        return data[value];
    }
}
