import {Injectable, NgZone} from '@angular/core';
import {Http, Headers, Response, RequestOptions} from '@angular/http';
import {config} from '../config';

import {Hotel} from '../interfaces/hotel';
import {Serializable} from '../interfaces/serializable';

import {Observable} from 'rxjs/Rx';


@Injectable()
export class Pricing {

    private _url = config.getEnvironmentVariable('endPoint') + 'admin/';
    private _curUrl: string;
    private _availableHotels: Array<Hotel> = [];

    constructor(public http: Http) {
    }

    getBar(hotel: any, page: number) {

        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('id_token'));
        let options = new RequestOptions({headers: headers});
        let url = this._url + hotel + '/pricing/bar?page=' + page;
        this._curUrl = this._url + hotel + '/pricing/bar';

        return this.http.get(url, options).map(this.setDefaultPricing).catch(this.handleError)

    }

    getDefault(hotel: any) {

        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('id_token'));
        let options = new RequestOptions({headers: headers});
        this._url = this._url + hotel + '/pricing/default';

        return this.http.get(this._url, options).map(this.setDefaultPricing).catch(this.handleError)

    }

    updateDefaultPricing(hotel: any, room_category_id: any, dow: any, min_val: any, max_val: any, default_val: any, default_price: any) {

        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('id_token'));
        let options = new RequestOptions({headers: headers});
        let parameters = {
            dow: dow,
            min_val: min_val,
            max_val: max_val,
            default_val: default_val,
            default_price: default_price
        };

        let body = JSON.stringify(parameters);

        return this.http.put(this._curUrl + '/' + room_category_id, body,
            options
        )
            .map(this.setDefaultPricing)
            .catch(this.handleError)

    }

    updateBarPricing(hotel: any, room_category_id: any, dt_stay: any, price_bar: any) {

        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('id_token'));
        let options = new RequestOptions({headers: headers});
        let parameters = {dt_stay: dt_stay, price_bar: price_bar};

        let body = JSON.stringify(parameters);

        return this.http.put(this._curUrl + '/' + room_category_id, body,
            options
        )
            .map(r => r.json())
            .catch(this.handleError)
    }

    removeBarPricing(hotel: any, room_category_id: any, dt_stay: any) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('id_token'));
        let parameters = {dt_stay: dt_stay};

        let options = new RequestOptions({headers: headers});

        return this.http.delete(this._curUrl + '/' + room_category_id + '/' + dt_stay,
            options
        )
            .map(r => r.json())
            .catch(this.handleError)

    }

    updateDiscPricing(hotel: any, room_category_id: any, dt_stay: any, price_disc: any) {
        this._curUrl = this._curUrl.replace('bar', 'disc');
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('id_token'));
        let options = new RequestOptions({headers: headers});
        let parameters = {dt_stay: dt_stay, price_disc: price_disc, price_disc_default: price_disc};

        let body = JSON.stringify(parameters);

        return this.http.put(this._curUrl + '/' + room_category_id, body,
            options
        )
            .map(r => r.json())
            .catch(this.handleError)

    }

    removeDiscPricing(hotel: any, room_category_id: any, dt_stay: any) {
        this._curUrl = this._curUrl.replace('bar', 'disc');
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('id_token'));
        //let parameters = {dt_stay: dt_stay, price_disc: price_disc, price_disc_default: price_disc};

        let options = new RequestOptions({headers: headers});

        return this.http.delete(this._curUrl + '/' + room_category_id + '/' + dt_stay,
            options
        )
            .map(r => r.json())
            .catch(this.handleError)

    }




    private setDefaultPricing(data) {
        let json = data.json();
        return json.sort((n1, n2) => n1.display_order - n2.display_order);
    }


    private handleError(error: any) {
        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg); // log to console instead
        return Observable.throw(errMsg);
    }

}

