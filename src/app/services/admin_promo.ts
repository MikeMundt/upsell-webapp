import {Injectable, NgZone} from '@angular/core';
import {Http, Headers, Response, RequestOptions} from '@angular/http';
import {config} from '../config';

import {Observable} from 'rxjs/Rx';


@Injectable()
export class AdminPromo {

    private _url = config.getEnvironmentVariable('endPoint') + 'admin/';

    constructor(public http: Http) {
    }

    getPromoList(hotel: any) {

        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('id_token'));
        let options = new RequestOptions({headers: headers});
        let url = this._url + hotel + '/promo';

        return this.http.get(url, options).map(r => r.json()).catch(this.handleError)
    }

    getPromo(hotel: any, promo_id: any) {

        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('id_token'));
        let options = new RequestOptions({headers: headers});
        let url = this._url + hotel + '/promo/' + promo_id;

        return this.http.get(url, options).map(r => r.json()).catch(this.handleError)
    }

    createPromo(hotel: any, promo_cd: string) {
        promo_cd = promo_cd.toUpperCase();
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('id_token'));
        let options = new RequestOptions({headers: headers});
        let url = this._url + hotel + '/promo/' + promo_cd;

        let body = JSON.stringify('');

        return this.http.post(url, body,
            options
        )
            .map(r => r.json())
            .catch(this.handleError);
    }

    updatePromo(hotel: any, promo_id: any, postObj: any) {
        if (postObj.customer_name)
            postObj.customer_name = postObj.customer_name.toUpperCase();
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('id_token'));
        let options = new RequestOptions({headers: headers});
        //let parameters = {room_category_id: room_category_id, dt_stay: dt_stay, avail_cd: avail_cd};
        let url = this._url + hotel + '/promo/' + promo_id;

        let body = JSON.stringify(postObj);

        return this.http.put(url, body,
            options
        )
            .map(r => r.json())
            .catch(this.handleError);
    }

    updatePromoRoomCategory(hotel: any, promo_id: any, promo_room_category_id: any, postObj: any) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('id_token'));
        let options = new RequestOptions({headers: headers});
        //let parameters = {room_category_id: room_category_id, dt_stay: dt_stay, avail_cd: avail_cd};
        let url = this._url + hotel + '/promo/' + promo_id + '/room_category/' + promo_room_category_id;

        let body = JSON.stringify(postObj);

        return this.http.put(url, body,
            options
        )
            .map(r => r.json())
            .catch(this.handleError);
    }


    private handleError(error: any) {
        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg); // log to console instead
        return Observable.throw(errMsg);
    }

}

