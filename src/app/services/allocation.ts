import {Injectable, NgZone} from '@angular/core';
import {Http, Headers, Response, RequestOptions} from '@angular/http';
import {config} from '../config';

import {Hotel} from '../interfaces/hotel';
import {Serializable} from '../interfaces/serializable';

import {Observable} from 'rxjs/Rx';


@Injectable()
export class Allocation {

    private _url = config.getEnvironmentVariable('endPoint') + 'admin/';
    private _availableHotels: Array<Hotel> = [];

    constructor(public http: Http) {
    }

    getAllocation(hotel: any, page: number) {

        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('id_token'));
        let options = new RequestOptions({headers: headers});
        let url = this._url + hotel + '/allocation/allocation?page=' + page;

        return this.http.get(url, options).map(this.setData).catch(this.handleError)

    }

    updateAllocation(hotel: any, room_category_id: string, dt_stay: string, allocation: number ) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('id_token'));
        let options = new RequestOptions({headers: headers});
        let parameters = {dt_stay: dt_stay, allocation: allocation};
        let url = this._url + hotel + '/allocation/allocation/' + room_category_id;

        //this._url = this._url+ hotel + '/availability'

        let body = JSON.stringify(parameters);

        return this.http.put(url, body,
            options
        )
            .map(r => r.json())
            .catch(this.handleError);
    }

    delAllocation(hotel: any, room_category_id: string, dt_stay: string ) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('id_token'));
        let options = new RequestOptions({headers: headers});
        let url = this._url + hotel + '/allocation/allocation/' + room_category_id + '/' + dt_stay;

        return this.http.delete(url,
            options
        )
            .map(r => r.json())
            .catch(this.handleError);
    }

    getAllocationDefault(hotel: any) {

        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('id_token'));
        let options = new RequestOptions({headers: headers});
        let url = this._url + hotel + '/allocation/default';

        return this.http.get(url, options).map(this.setData).catch(this.handleError)

    }

    updateAllocationDefault(hotel: any, room_category_id: string, dow: number, default_allocation: number ) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('id_token'));
        let options = new RequestOptions({headers: headers});
        let parameters = {dow: dow, default_allocation: default_allocation};
        let url = this._url + hotel + '/allocation/default/' + room_category_id;

        //this._url = this._url+ hotel + '/availability'

        let body = JSON.stringify(parameters);

        return this.http.put(url, body,
            options
        )
            .map(r => r.json())
            .catch(this.handleError);
    }


    private setData(data) {
        //console.log(data.json().sort((n1, n2) => n1.display_order - n2.display_order));
        return data.json().sort((n1, n2) => n1.display_order - n2.display_order);
    }


    private handleError(error: any) {
        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg); // log to console instead
        return Observable.throw(errMsg);
    }

}

