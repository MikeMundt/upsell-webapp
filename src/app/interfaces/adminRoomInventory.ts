import {Serializable} from './serializable';

import {inventoryLevel} from '../helpers';

export class AdminRoomInventory extends Serializable {
    id: string;
    hotel_id: string;
    tenant_id: string;
    room_category_name: string;
    room_category_id: string;
    inventory_date: Date;
    inventory_level: inventoryLevel;

    updateInventoryLevel() {
        switch (this.inventory_level) {
            case inventoryLevel.Open:
                this.inventory_level = inventoryLevel.Low;
                break;
            case inventoryLevel.Low:
                this.inventory_level = inventoryLevel.VeryLow;
                break;
            case inventoryLevel.VeryLow:
                this.inventory_level = inventoryLevel.Closed;
                break;
            case inventoryLevel.Closed:
                this.inventory_level = inventoryLevel.Open;
                break;
            default:
                alert("You shouldn't see this - error in interfaces/AdminRoomInventory");
        }
    }
}
