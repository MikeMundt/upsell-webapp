import { Component, OnInit,OnChanges, AfterContentChecked } from '@angular/core';
import { config } from './config';
import { AuthService } from './services/auth_service';
import { Shared }   from './services/shared';
import { Hotel } from './interfaces/hotel';
import { TitleCasePipe } from './pipes/title-case.pipe'

import {
  CanActivate,
  Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  ActivatedRoute
} from '@angular/router';

@Component({
  moduleId: module.id,
  selector: 'my-app',
  templateUrl: 'templates/container.html',
  providers: [AuthService],
})
export class AppComponent implements OnInit,AfterContentChecked{
  username: string;
  currentSelect: string;
  hotels: Hotel[];
  conf_enabled: boolean;
  show_path_panel: boolean;
  show_hotel_dropdown: boolean;
  admin: boolean;
  is_frontend_route: boolean;
  is_admin_route: boolean;
  isLoading: boolean;

  constructor(private auth: AuthService, private shared: Shared, private router: Router, private route: ActivatedRoute) {
    this.isLoading = false;
  }

  ngOnInit() {
    this.shared.config = this.router.config;
  }



  ngAfterContentChecked() {
    this.username = this.shared.username ;
    this.hotels = this.shared.availableHotels;
    if (this.auth.isAdmin()) {
      this.admin = true;
    }
    if (this.hotels) {
      this.currentSelect = this.hotels.filter(hotel => hotel.hotel_id === this.shared.selectedHotel).length ? this.hotels.filter(hotel => hotel.hotel_id === this.shared.selectedHotel)[0].hotel_name  : this.hotels[0].hotel_name;
    }
    // Remove the links on the offer/hotel select page
    this.show_path_panel = this.router.url.indexOf('offer') != -1 ? false : this.router.url.indexOf('hotels') != -1 ? false: this.router.url.indexOf('admin') != -1 ? false: this.router.url.indexOf('profile') != -1 ? false :true;
    this.show_hotel_dropdown =  this.router.url.indexOf('hotels') != -1 ? false: true;

    if (this.router.url.indexOf('admin') != -1) {
      this.is_frontend_route = false;
      this.is_admin_route = true;
    } else {
      this.is_frontend_route = true;
      this.is_admin_route = false;
    }

  }



  goAdmin() {

    //this.shared.username = null
    this.router.navigate(["/admin/inventory"]);
  }

  goHome() {
    this.shared.reset();
    this.shared.show_res_conf = false;
    this.router.navigate(["generic", this.shared.selectedHotel]);
  }

  userChangePassword() {
    this.router.navigate(["/profile"]);
  }

  goConf() {

    //this.shared.username = null
    this.shared.show_res_conf = true;
    this.router.navigate(["/confirm"]);


  }

  enablePromo(event) {
    this.isLoading = true;
    this.shared.usePromo = true;
    let that = this;
    setTimeout(function(){
      that.isLoading = false;
    }, 100);

    //event.preventDefault();
    //this.shared.last_url = this.router.url;
    //this.router.navigate(["/confirm"])
  }

  disablePromo(event) {
    this.isLoading = true;
    this.shared.usePromo = false;
    let that = this;
    setTimeout(function(){
      that.isLoading = false;
    }, 100);
    //event.preventDefault();
    //this.shared.reset();
    //this.conf_enabled = false;
    //if (this.shared.last_url == this.router.url){
    //  this.shared.last_url = '/hotel/' + this.shared.selectedHotel
    //}

    //this.router.navigate([this.shared.last_url ? this.shared.last_url : '/hotel/' + this.shared.selectedHotel])
  }

  clickedBreadcrumb(event) {
    this.router.navigate([event.target.href]);
  }

  clickedBar(hotelId: string, event) {
    //event.preventDefault();
    this.shared.reset();
    this.shared.selectedHotel = hotelId;

    if (this.is_admin_route) {
      this.router.navigate(["admin"]);
    } else {
      this.router.navigate(["generic", hotelId]);

    }
  }

  logOut() {
    localStorage.clear();
    this.username = '';
    this.router.navigate(['/']);
  }

}

