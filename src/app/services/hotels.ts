import {Injectable, NgZone} from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { config } from '../config';

import { Hotel } from '../interfaces/hotel';
import { Serializable } from '../interfaces/serializable';

import { Observable } from 'rxjs/Rx';


@Injectable()
export class Hotels {

  private _url = config.getEnvironmentVariable('endPoint')+'hotel/';
  private _availableHotels: Array<Hotel> = [];

  constructor(public http: Http) {
  }

  getHotels() {

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', 'Bearer ' + localStorage.getItem('id_token'));
    let options = new RequestOptions({ headers: headers });


    return this.http.get(this._url,
      options
      )
      .map(this.setNewHotels)
      .catch(this.handleError);

  }

  private setNewHotels(data){
      let tempHotels: Array<Hotel> = [];

      for (var datum of data.json()) {
          let tempHotel = new Hotel();
          let s = new Serializable();
          tempHotel.fillFromObject(datum);
          tempHotels.push(tempHotel);
      }


      this.availableHotels = tempHotels;
      return tempHotels;
  }

    get availableHotels(): Array<Hotel> {
        return this._availableHotels;
    }
    set availableHotels(newHotelList: Array<Hotel>) {
        this._availableHotels = newHotelList;
    }

  private handleError (error: any) {
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg); // log to console instead
    return Observable.throw(errMsg);
  }

}

