import {Injectable, NgZone} from '@angular/core';
import {Observable} from 'rxjs/Rx';

import { Hotel } from '../interfaces/hotel';

@Injectable()
export class Shared {

  //public availableHotels = [];
  public availableClasses = [];
  // public selectedHotel;
  public selectedClass;
  public selectedRoomType;
  public selectedLengthOfStay;
  public selectedSourceCodeLetter;
  public selectedSourceCode;
  public sourceCodes = [];
  public firstLetters = [];
  // public username: string;
  public last_url;
  public show_res_conf;
  public current_row: number;
  public current_column: number;
  public config = [];
  public origConfig = [];
  public current_answers;
  public path = {};
  public params = {};
  public tree;
  public flat = [];
  public bread = [];
  public storedGenericRoutes = false;
  public usePromo = false;
  public promoCode = '';
  public quickClickEnabled = false;
  public quickClickData;

  public flgAutopilot = true;

  constructor(){
    this.current_row = 0;
    this.current_column = 0;
  }

  get username(): string {
    return localStorage.getItem('username');
  }
  set username(_username: string) {
    localStorage.setItem('username', _username);
  }

  get selectedHotel(): string {
    return localStorage.getItem('selectedHotel');
  }
  set selectedHotel(_selectedHotel: string) {
    localStorage.setItem('selectedHotel', _selectedHotel);
  }

  get availableHotels(): Array<Hotel> {
    return JSON.parse(localStorage.getItem('availableHotels'));
  }
  set availableHotels(_availableHotels: Array<Hotel>) {
    localStorage.setItem('availableHotels', JSON.stringify(_availableHotels));
  }

  reset(){
    this.availableClasses = [];
    this.selectedClass = void 0;
    this.selectedRoomType = void 0;
    this.selectedLengthOfStay = void 0;
    this.selectedSourceCodeLetter = void 0;
    this.selectedSourceCode = void 0;
    this.sourceCodes = [];
    this.firstLetters = [];
    this.current_row = 0;
    this.current_column = 0;
    this.path = [];
    this.params = [];
    this.tree = void 0;
    this.flat = [];
    this.bread = [];
    this.storedGenericRoutes = false;
    this.usePromo = false;
    this.promoCode = '';
    this.quickClickData = void 0;
    this.show_res_conf = false;
  }
}
