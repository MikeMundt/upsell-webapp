import { Component, OnInit, Injectable, ViewChild, NgZone } from '@angular/core';
import { Account } from '../services/account'
import { config } from '../config'
import {
    CanActivate,
    Router,
    ActivatedRouteSnapshot,
    RouterStateSnapshot,
    ActivatedRoute
} from '@angular/router';

import { Shared }   from '../services/shared';

declare var module: any; //needed for moduleId
@Component({
    moduleId: module.id,
    selector: 'recovery',
    templateUrl: '../templates/recovery.template.html',
    providers: [Account]
})
export class RecoveryComponent implements OnInit {
    errorMessage: string = '';
    tokenid: string;
    validToken: boolean = false;
    newPassword: string;
    confirmPassword: string;
    successful: boolean = false;
    constructor(private accountService : Account, private router: Router, private shared: Shared, private activatedRoute: ActivatedRoute) {
        this.accountService = accountService;
        this.router = router;
        this.shared = shared;
    }

    ngOnInit() {
        this.activatedRoute.params.subscribe(params => {
            this.init(Object.assign({},params));
        })
    }

    init(initParams: any) {
        if (initParams && initParams.token_id) {
            this.tokenid = initParams.token_id;

            this.accountService.tokenValidation(this.tokenid).subscribe( () => {
                    this.validToken = true;
                },
                error =>  {

                })
        } else {
            this.router.navigate(['/']);
        }

    }

    goHome() {
        this.router.navigate(['/']);
    }

    savePassword() {
        this.accountService.tokenSubmit(this.tokenid, this.newPassword).subscribe( () => {
                this.successful = true;
            },
            error =>  {
                this.errorMessage = ""
            })
    }

}
