import {Component, OnInit, OnChanges, NgZone} from '@angular/core';

import {Shared} from '../../services/shared'
import {Allocation} from '../../services/allocation'

import {AdminRoomInventory} from '../../interfaces/adminRoomInventory';

import {
    CanActivate,
    Router,
    NavigationEnd,
    ActivatedRouteSnapshot,
    RouterStateSnapshot,
    ActivatedRoute
} from '@angular/router';

declare var module: any; //needed for moduleId
declare var $: any;

@Component({
    moduleId: module.id,
    selector: 'luxe-admin-allocation',
    templateUrl: "../../templates/admin/admin_allocation.template.html",
    providers: [Allocation]
})
export class AdminAllocationComponent implements OnInit, OnChanges {
    roomInventory: AdminRoomInventory[];
    inventoryDates: Date[];
    roomCategories: String[];
    columnData: any;
    rowData: any;
    errorMessage: any;
    cells: any;
    page: number;

    constructor(private shared: Shared, private allocation: Allocation, private zone: NgZone, private router: Router) {
        this.shared = shared;
        this.allocation = allocation;
        this.page = 0;

        // override the route reuse strategy
        this.router.routeReuseStrategy.shouldReuseRoute = function(){
            return false;
        }

        this.router.events.subscribe((evt) => {
            if (evt instanceof NavigationEnd) {
                // trick the Router into believing it's last link wasn't previously loaded
                this.router.navigated = false;
                // if you need to scroll back to top, here is the right place
                //window.scrollTo(0, 0);
            }
        });
    }

    ngOnInit() {
        this.refreshData();
    }

    refreshData() {
        let that = this;
        this.allocation.getAllocation(this.shared.selectedHotel, this.page).subscribe(
            inventory => {
                this.columnData = inventory;
                inventory.map(function (row) {
                    row.daily_values.map(function (col) {

                        col.color = "";

                        let alloc = col.allocation_ovrd ? col.allocation_ovrd : col.allocation;
                        if ((col.num_upsells / alloc) > .8)
                            col.color = "orange";

                        if ((col.num_upsells / alloc) >= 1)
                            col.color = "red";
                    });
                });
                this.rowData = inventory[0].daily_values.map(function (x) {
                    //return x.dt_stay.split('-')[1].replace("0", "") + "/" + x.dt_stay.split('-')[2];
                    let d = new Date(x.dt_stay);
                    let f = (d.getMonth() + 1) + "/" + (d.getDate() + 1);
                    return f;
                });
            },
            error => this.errorMessage = <any>error);
    }

    checkBackground(date: any) {
        return date.allocation_ovrd ? 'rgb(38, 38, 38)' : '';
    }

    checkEnter(event) {
        if (event.keyCode === 13) {
            event.target.blur();
            event.preventDefault();
        }
    }

    navPaging(direction: string) {
        switch (direction)
        {
            case 'up':
                this.page++;
                break;
            case 'down':
                this.page--;
                break;
        }

        if (this.page < 0) this.page = 0;

        this.refreshData();
    }

    clicked(event, roomCategory: any, date: any) {
        if (!event) event = window.event;
        let tar = event.target || event.srcElement;
        let new_value = tar.innerText;

        let that = this;
        let alloc = date.allocation_ovrd ? date.allocation_ovrd : date.allocation;
        if (isNaN(new_value) || Number(new_value) == Number(alloc)) {
            tar.innerText = '...';
            that.reloadPage();
            return false;
        }


        if (!isNaN(parseFloat(new_value)) && isFinite(new_value)) {
            this.allocation.updateAllocation(this.shared.selectedHotel, roomCategory.room_category_id, date.dt_stay, new_value).subscribe(
                d => {
                    tar.innerText = '...';
                    that.reloadPage();
                },
                error => this.errorMessage = <any>error);
        } else {

         this.allocation.delAllocation(this.shared.selectedHotel, roomCategory.room_category_id, date.dt_stay).subscribe(
         d => {
             tar.innerText = '...';
             that.reloadPage();
         },
         error => this.errorMessage = <any>error);

         }
    }


    ngOnChanges() {
    /*    for (let j of this.roomInventory) {
            this.inventoryDates.push(j.inventory_date)
        }

        for (let j of this.roomInventory) {
            this.roomCategories.push(j.room_category_name)
        }

        this.inventoryDates = this.inventoryDates.sort(function (a, b) {
            a = new Date(a);
            b = new Date(b);
            return a > b ? -1 : a < b ? 1 : 0;
        });

        this.roomCategories = this.roomCategories.sort();

        this.inventoryDates.filter(function (value, index, self) {
            return self.indexOf(value) === index;
        });
        this.roomCategories.filter(function (value, index, self) {
            return self.indexOf(value) === index;
        });

        this.columnData = {"prop": "id"};

        for (let j of this.inventoryDates) {
            this.columnData.push({"name": this.formatDate(j)});
        }
*/

    }

    highlight(event:any, date: any) {
        event.preventDefault();
        let tar = event.currentTarget;
        tar.innerText = date.allocation_ovrd ? date.allocation_ovrd : date.allocation;


        var range = document.createRange();
        range.selectNodeContents(event.currentTarget);
        var sel = window.getSelection();
        sel.removeAllRanges();
        sel.addRange(range);
        event.currentTarget.focus();
    }

    formatDate(unformattedDate: Date) {
        let month = '' + (unformattedDate.getMonth() + 1);
        let day = '' + unformattedDate.getDate();
        let year = unformattedDate.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join('-');
    }

    reloadPage() {
       /* this.zone.runOutsideAngular(() => {
            location.reload();
        });*/
        this.router.navigate(["/admin/allocation"]); /*, { queryParams: { 'refresh': 1 } }*/
    }
}
